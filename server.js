// load the things we need
var fs = require('fs');
var busboy = require('connect-busboy');
var multer = require('multer');

const upload = multer({ dest: '/tmp/' });

var express = require('express');
var session = require('express-session');
var path = require('path');
var utils = require('./lib/utils.js');
var distancia = require('./lib/distancia.js');
var http = require('http');

var nodeMailer = require('nodemailer'),
    bodyParser = require('body-parser');

var app = express();
// set the view engine to ejs
app.set('view engine', 'ejs');
var config = require('./config.json');

app.use(config.url, express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(busboy());
var port = utils.normalizePort(process.env.PORT || config.port);
var server;

// payments
var keyPublishable = 'pk_test_PqLOjFLG2KBt8qXprJbwSB75';
var keySecret = 'sk_test_qbFzxKfOyP8zFNDUB0GoVWpJ';
var stripe = require('stripe')(keySecret);

const pg = require('pg');
const opt_db = {
  //user: 'postgres',
  user: 'autosmart',
  host: 'localhost',
  database: 'autosmart',
  password: 'eYrQQQwHP9UbJRFP',
  //password: "J-7V5?Qx'2-wwS/_k=9f,",
  port: '5432'};


//app.use(session({secret: 'autosmart'}));
app.use(session({ secret: 'Qx2-wwS_', cookie: { maxAge: 18000000 }}))

app.set('port', port);

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("dAccess-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


function showDate() {

    var today = new Date();
    var today_ = today;

    var dd = today_.getDate();
    var mm = today_.getMonth()+1;
    var yyyy = today_.getFullYear();

    if(dd<10) {
        dd = '0'+dd;
    }

    if(mm<10) {
        mm = '0'+mm;
    }

    var today_ = yyyy+"-"+mm+"-"+dd;

    return today_;

}

function showTime() {
  var timeNow = new Date();
  var hours   = timeNow.getHours();
  var minutes = timeNow.getMinutes();
  var seconds = timeNow.getSeconds();
  var timeString = "" + ((hours > 12) ? hours - 12 : hours);
  timeString  += ((minutes < 10) ? ":0" : ":") + minutes;
  timeString  += ((seconds < 10) ? ":0" : ":") + seconds;
  //timeString  += (hours >= 12) ? " P.M." : " A.M.";
  return timeString;
}

function send_mail(asunto,msg,email){
    let transporter = nodeMailer.createTransport({
        host: 'smtp.autosmart.pe',
        secure: false, //disable SSL
        requireTLS: true, //Force TLS
        tls: {
            rejectUnauthorized: false
        },
        port: 587, //Port of STMP service
        auth: {
            user: 'autosmart_c@autosmart.pe',
            pass: '#kpNlbs3'
        }
    });

    // setup email data with unicode symbols
    let mailOptions = {
        from: '"AutoSmart" <autosmart_c@autosmart.pe>', // sender address
        to: email, // list of receivers
        subject: asunto,
        //text: req.body.mensaje,
        html: msg
    };

    transporter.sendMail(mailOptions, (error, info) => {

        if (error) {
            return false;
        }else{
            return true;
        }

    });
}


// index page
app.get('/', function(req, res) {

	res.render('pages/index_c');

});

app.get('/index', function(req, res) {

  res.render('pages/index');

});

//lista de paquetes
app.get('/lista_paquetes', function(req, res) {

  console.log("entro");

	(async function() {

		const pool = new pg.Pool(opt_db);

		pool.query("select * from paquetes", (err, result) => {

		      if(err){
		        var info = {'status':'fail', 'data':''};
		        console.log(info);
		        res.json(info);
		      }else{
		        var info = {'status':'ok', 'data':result.rows};
		        console.log(info);
		        res.json(info);
		      }

		      pool.end();

		});
	})();

});


// nosotros page
app.get('/nosotros', function(req, res) {
	res.render('pages/nosotros');
});

//Servicios page
app.get('/servicios', function(req, res) {
	res.render('pages/servicios');
});

//contactanos
app.get('/contactanos', function(req, res) {
	res.render('pages/contactanos');
});

// send mail contact
app.post('/send-email-contact', function (req, res) {

    let transporter = nodeMailer.createTransport({
      host: 'smtp.autosmart.pe',
      secure: false, //disable SSL
      requireTLS: true, //Force TLS
        tls: {
          rejectUnauthorized: false
        },
        port: 587, //Port of STMP service
        auth: {
          user: 'autosmart_c@autosmart.pe',
          pass: '#kpNlbs3'
         }
      });

    // setup email data with unicode symbols
    let mailOptions = {
        from: '"AutoSmart" <autosmart_c@autosmart.pe>', // sender address
        to: 'autosmart_c@autosmart.pe', // list of receivers
        subject: "Contactanos Autosmart.pe",
        text: req.body.mensaje,
        html: '<b>Email enviado desde Autosamrt</b>'
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        var info = {'status':'fail'};

        if (error) {
             return res.json(info);
        }

        info = {'status':'ok'};
        return res.json(info);
    });


});

app.post('/save_steps', function (req, res) {




    info = {'status':'ok'};
    return res.json(info);

});


/* Modulo Login Admin */
app.get('/admin/login', function (req, res) {

    res.render('pages/admin/login');

});

app.post('/admin/login_a', function (req, res) {


  (async function() {

    const pool = new pg.Pool(opt_db);

    const sql = 'select u.nombres, p.nombre, u.correo, u.ideempresa, u.ideusuario from usuario u inner join perfil p on p.ideperfil=u.ideperfil where u.nombres=$1 and u.password=$2';
    const values = [req.body.usuario,req.body.password];

    pool.query(sql,values, (err, result) => {

          if(err){

            console.log(err);
            var info = {'status':'fail'};
            console.log(info);
            return res.json(info);

          }else{

            //console.log(result.rows[0]);

            req.session.coduser = result.rows[0].ideusuario;
            req.session.username = result.rows[0].nombres;
            req.session.perfil = result.rows[0].nombre;

            info = {'status':'ok'};
            return res.json(info);
          }

          pool.end();

    });
  })();






});

app.get('/admin/salir', function (req, res) {

    delete req.session.coduser;

    res.render('pages/admin/login');

});
/* -----  ----- */



/* Modulo Home Admin */

app.get('/admin/home', function (req, res) {

    if(req.session.coduser){
        //res.render('pages/admin/home', { username: req.session.username });
        res.render('pages/admin/home', { username: req.session.username, perfil: req.session.perfil });
    }else{
       res.render('pages/admin/login');
    }


});

/* -----  ----- */


/* Modulo Empresa */
app.get('/admin/empresa', function (req, res) {

    if(req.session.coduser){
        res.render('pages/admin/empresa', { username: req.session.username, perfil: req.session.perfil });
    }else{
       res.render('pages/admin/login');
    }


});

app.post('/admin/lista_empresa', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          const sql = 'select ideempresa, nombre, direccion, telefono, estado , correo1 from empresa where estado=$1';
          const values = ['A'];

          pool.query(sql,values, (err, result) => {

                if(err){

                  var info = {'status':'fail'};
                  return res.json(info);

                }else{

                  //console.log(result.rows);

                  info = {'status':'ok','datos':result.rows};
                  return res.json(info);
                }

                pool.end();


          });
        })();

    }else{

       info = {'status':'fail'};
       return res.json(info);
    }



});

app.post('/admin/get_empresa', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          const sql = 'select ideempresa, nombre, direccion, telefono, estado , correo1, correo2, correo3 ,coddist, coordenada_x, coordenada_y from empresa where estado=$1 and ideempresa=$2';
          const values = ['A', req.body.cod_empresa];

          pool.query(sql,values, (err, result) => {

                if(err){

                  var info = {'status':'fail'};
                  return res.json(info);

                }else{

                  //console.log(result.rows);

                  info = {'status':'ok','datos':result.rows};
                  return res.json(info);
                }

                pool.end();


          });
        })();

    }else{

       info = {'status':'fail'};
       return res.json(info);
    }



});

app.post('/admin/get_empresas', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          const sql = 'select ideempresa, nombre from empresa where estado=$1';
          const values = ['A'];

          pool.query(sql,values, (err, result) => {

                if(err){

                  var info = {'status':'fail'};
                  return res.json(info);

                }else{

                  //console.log(result.rows);

                  info = {'status':'ok','datos':result.rows};
                  return res.json(info);
                }

                pool.end();


          });
        })();

    }else{

       info = {'status':'fail'};
       return res.json(info);
    }



});

app.post('/admin/new_empresa', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          var fecha_act = showDate();
          var hora_envio = showTime();

          console.log(fecha_act)
          console.log(hora_envio)

          const text = 'INSERT INTO empresa(nombre,direccion,telefono,correo1,correo2,correo3,coordenada_x,coordenada_y,coddist,estado,feccrea,horcrea,fecmodif,hormodif,usrcrea,usrmodif) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16) RETURNING *'
          const values = [req.body.nombre,req.body.direccion,req.body.telefono,req.body.correo1,req.body.correo2,req.body.correo3,req.body.cordenadasx,req.body.cordenadasy,req.body.distrito,"A",fecha_act,hora_envio,fecha_act,hora_envio,req.session.coduser,req.session.coduser]

          // callback
          pool.query(text, values, (err, result) => {

            if (err) {
               console.log(err)
              console.log(err.stack)
              info = {'status':'fail','datos':''};
              return res.json(info);

            } else {

              //console.log(res.rows[0])
              // { name: 'brianc', email: 'brian.m.carlson@gmail.com' }
              info = {'status':'ok','datos':''};
              return res.json(info);

            }

          });

       })();


    }else{
       res.render('pages/admin/login');
    }


});


app.post('/admin/edit_empresa', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          var fecha_act = showDate();
          var hora_envio = showTime();

          const text = 'update empresa set nombre=$1,direccion=$2,telefono=$3,correo1=$4,correo2=$5,correo3=$6,coordenada_x=$7,coordenada_y=$8,coddist=$9,fecmodif=$10,hormodif=$11,usrmodif=$12 where ideempresa=$13';
          const values = [req.body.nombre,req.body.direccion,req.body.telefono,req.body.correo1,req.body.correo2,req.body.correo3,req.body.cordenadasx,req.body.cordenadasy,req.body.distrito,fecha_act,hora_envio,req.session.coduser,req.body.ideempresa];

          // callback
          pool.query(text, values, (err, result) => {

            if (err) {
               console.log(err)
              console.log(err.stack)
              info = {'status':'fail','datos':''};
              return res.json(info);

            } else {

              //console.log(res.rows[0])
              // { name: 'brianc', email: 'brian.m.carlson@gmail.com' }
              info = {'status':'ok','datos':''};
              return res.json(info);

            }

          });

       })();


    }else{
       res.render('pages/admin/login');
    }


});




app.post('/admin/delete_empresa', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          const text = 'delete from empresa where ideempresa=$1'
          const values = [req.body.cod_empresa]

          // callback
          pool.query(text, values, (err, result) => {

            if (err) {
              console.log(err)
              console.log(err.stack)
              info = {'status':'fail','datos':''};
              return res.json(info);

            } else {

              info = {'status':'ok','datos':''};
              return res.json(info);

            }

          });

       })();


    }else{
       res.render('pages/admin/login');
    }


});

/* -----  ----- */



/* Modulo Oficina */
app.get('/admin/oficina', function (req, res) {

    if(req.session.coduser){


        res.render('pages/admin/oficina', { username: req.session.username, perfil: req.session.perfil });
    }else{
       res.render('pages/admin/login');
    }


});


app.post('/admin/lista_oficina', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          const sql = 'select o.ideoficina, o.nombre, o.direccion, o.telefono, o.estado,e.nombre as empresa from oficina o inner join empresa e on e.ideempresa=o.ideempresa where o.estado=$1';
          const values = ['A'];

          pool.query(sql,values, (err, result) => {

                if(err){
                  console.log(err)
                  var info = {'status':'fail'};
                  return res.json(info);

                }else{

                  //console.log(result.rows);

                  info = {'status':'ok','datos':result.rows};
                  return res.json(info);
                }

                pool.end();


          });
        })();

    }else{

       info = {'status':'fail'};
       return res.json(info);
    }



});


app.post('/admin/get_oficina', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          const sql = 'select ideempresa, nombre, direccion, telefono, estado, coddist, coordenada_x, coordenada_y, ideempresa, longitud, latitud, tipo, foto from oficina where estado=$1 and ideoficina=$2';
          const values = ['A', req.body.codigo];

          pool.query(sql,values, (err, result) => {

                if(err){

                  var info = {'status':'fail'};
                  return res.json(info);

                }else{

                  //console.log(result.rows);

                  info = {'status':'ok','datos':result.rows};
                  return res.json(info);
                }

                pool.end();


          });
        })();

    }else{

       info = {'status':'fail'};
       return res.json(info);
    }



});



app.post('/admin/new_oficinaqqqqq', upload.any(), function (req, res, next) {


  try{

    console.log(req.body.direccion);
    console.log(req.files);

    const file = req.files[0];
    fs.readFile(file.path, function(err, data) {
      if (err)
        throw err;
        var fileName = "info.jpg";
        var newPath = __dirname + '/images/' + fileName;
        fs.writeFile(newPath, data, function (error) {
          if (error) {
            console.log(error);
            //res.end();
          } else {
            //res.end(fileName);
          }
        });
    });

  }catch(error) {


  }


 /*
  const file = req.files[0];
  fs.readFile(file.path, function(err, data) {
    if (err)
      throw err;
    res.send(`Your files's name is ${file.originalname}. Its size is ${file.size} and its mime type is ${file.mimetype}`);
  });


  var fstream;
  req.pipe(req.busboy);
  req.busboy.on('file', function (fieldname, file, filename) {
      console.log("Uploading: " + filename);
      fstream = fs.createWriteStream(__dirname + '/images/' + filename);
      file.pipe(fstream);
      fstream.on('close', function () {
          //res.redirect('back');
      });
  });
 */
  info = {'status':'ok','datos':''};
  return res.json(info);

});

app.post('/admin/new_oficina', upload.any(), function (req, res, next) {

    if(req.session.coduser){

        (async function() {

          var fileName = "";

          try{

            console.log(req.body);
            console.log(req.files);

            //############################

            var d_act = new Date();
            var tk = Math.floor(Math.random() * 9000) + 2;
            var num_img = "autosmart_"+d_act.getTime()+""+tk;

            fileName = num_img+".jpg";

            const file = req.files[0];
            fs.readFile(file.path, function(err, data) {
              if (err)
                throw err;


                var newPath = __dirname + '/public/produccion/img/oficina/' + fileName;
                fs.writeFile(newPath, data, function (error) {
                  if (error) {
                    console.log(error);
                    //res.end();
                  } else {
                    //res.end(fileName);
                  }
                });
            });

          }catch(error) {


          }

          //############################


          const pool = new pg.Pool(opt_db);


          var fecha_act = showDate();
          var hora_envio = showTime();

          const text = 'INSERT INTO oficina(nombre,direccion,telefono,ideempresa,coordenada_x,coordenada_y,coddist,estado,feccrea,horcrea,fecmodif,hormodif,usrcrea,usrmodif,longitud,latitud,tipo,foto) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18) RETURNING *'
          const values = [req.body.nombre,req.body.direccion,req.body.telefono,req.body.cod_empresa,req.body.cordenadasx,req.body.cordenadasy,req.body.distrito,"A",fecha_act,hora_envio,fecha_act,hora_envio,req.session.coduser,req.session.coduser, req.body.longitud, req.body.latitud, req.body.tipo,fileName]

          // callback
          pool.query(text, values, (err, result) => {

            if (err) {
               console.log(err)
              console.log(err.stack)
              info = {'status':'fail','datos':''};
              return res.json(info);

            } else {

              var ideoficina_new = result.rows[0].ideoficina;

              const text_h = 'INSERT INTO horarioatencion(fecha,ideoficina,h_7_00,h_8_00,h_9_00,h_10_00,h_11_00,h_12_00,h_13_00,h_14_00,h_15_00,h_16_00,h_17_00,h_18_00,h_19_00,h_20_00,estado,feccrea,horcrea,fecmodif,hormodif,usrcrea,usrmodif, tipo) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24) RETURNING *'
              const values_h = ["Lun-Vier",ideoficina_new,"1","1","1","1","1","1","1","1","1","1","1","1","1","1","A",fecha_act,hora_envio,fecha_act,hora_envio,req.session.coduser,req.session.coduser,'D']

              pool.query(text_h, values_h, (err, result) => {

                if (err) {
                   console.log(err)
                  console.log(err.stack)
                  info = {'status':'fail','datos':''};
                  return res.json(info);

                } else {

                  const text_h1 = 'INSERT INTO horarioatencion(fecha,ideoficina,h_7_00,h_8_00,h_9_00,h_10_00,h_11_00,h_12_00,h_13_00,h_14_00,h_15_00,h_16_00,h_17_00,h_18_00,h_19_00,h_20_00,estado,feccrea,horcrea,fecmodif,hormodif,usrcrea,usrmodif, tipo) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24) RETURNING *'
                  const values_h1 = ["Sab-Dom",ideoficina_new,"0","1","1","1","1","1","1","1","1","1","0","0","0","0","A",fecha_act,hora_envio,fecha_act,hora_envio,req.session.coduser,req.session.coduser,'D']

                  pool.query(text_h1, values_h1, (err, result) => {

                    if (err) {
                      console.log(err)
                      console.log(err.stack)
                      info = {'status':'fail','datos':''};
                      return res.json(info);

                    } else {

                      info = {'status':'ok','datos':''};
                      return res.json(info);

                    }

                    pool.end();

                  });

                }



              })


            }


          });

       })();


    }else{
       res.render('pages/admin/login');
    }


});


app.post('/admin/edit_oficina', upload.any(), function (req, res, next) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          var fecha_act = showDate();
          var hora_envio = showTime();

          var fileName = "";

          try{

            console.log(req.body);
            console.log(req.files);

            //############################
            var d_act = new Date();
            var tk = Math.floor(Math.random() * 9000) + 2;
            var num_img = "autosmart_"+d_act.getTime()+""+tk;

            fileName = num_img+".jpg";

            const file = req.files[0];
            fs.readFile(file.path, function(err, data) {
              if (err)
                throw err;


                var newPath = __dirname + '/public/produccion/img/oficina/' + fileName;
                fs.writeFile(newPath, data, function (error) {
                  if (error) {
                    console.log(error);
                    //res.end();
                  } else {
                    //res.end(fileName);
                  }
                });
            });

          }catch(error) {

            fileName = req.body.fileName;

          }
          //############################


          //const text = 'INSERT INTO empresa(nombre,direccion,telefono,correo1,correo2,correo3,coordenada_x,coordenada_y,coddist,estado,feccrea,horcrea,fecmodif,hormodif,usrcrea,usrmodif) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16) RETURNING *'
          const text = 'update oficina set nombre=$1,direccion=$2,telefono=$3,coordenada_x=$4,coordenada_y=$5,coddist=$6,ideempresa=$7,fecmodif=$8,hormodif=$9,usrmodif=$10,longitud=$11,latitud=$12,tipo=$13,foto=$14 where ideoficina=$15';
          const values = [req.body.nombre,req.body.direccion,req.body.telefono,req.body.cordenadasx,req.body.cordenadasy,req.body.distrito,req.body.cod_empresa,fecha_act,hora_envio,req.session.coduser,req.body.longitud,req.body.latitud,req.body.tipo,fileName,req.body.codigo];

          // callback
          pool.query(text, values, (err, result) => {

            if (err) {
               console.log(err)
              console.log(err.stack)
              info = {'status':'fail','datos':''};
              return res.json(info);

            } else {

              //console.log(res.rows[0])
              // { name: 'brianc', email: 'brian.m.carlson@gmail.com' }
              info = {'status':'ok','datos':''};
              return res.json(info);

            }

          });

       })();


    }else{
       res.render('pages/admin/login');
    }


});




app.post('/admin/delete_oficina', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          const text = 'delete from oficina where ideoficina=$1'
          const values = [req.body.codigo]

          // callback
          pool.query(text, values, (err, result) => {

            if (err) {
              console.log(err)
              console.log(err.stack)
              info = {'status':'fail','datos':''};
              return res.json(info);

            } else {

              const text_h = 'delete from horarioatencion where ideoficina=$1'
              const values_h = [req.body.codigo]

              pool.query(text_h, values_h, (err, result) => {

                if (err) {

                  console.log(err)
                  console.log(err.stack)

                  info = {'status':'fail','datos':''};
                  return res.json(info);

                } else {


                  info = {'status':'ok','datos':''};
                  return res.json(info);

                }

              });



            }

          });

       })();


    }else{
       res.render('pages/admin/login');
    }


});

/* -----  ----- */

/* Modulo Horarios */

app.get('/admin/horarios', function (req, res) {

    if(req.session.coduser){

        var codigo = req.query.codigo;
        var nombre = req.query.nombre;

        res.render('pages/admin/horarios', { username: req.session.username, perfil: req.session.perfil, codigo_of: codigo ,nombre: nombre});

    }else{
       res.render('pages/admin/login');
    }


});

app.post('/admin/lista_horarios', function (req, res) {

    if(req.session.coduser){

        console.log(req.body.codigo_of);

        (async function() {

          const pool = new pg.Pool(opt_db);

          const sql = 'select idehorario,fecha,h_7_00,h_8_00,h_9_00,h_10_00,h_11_00,h_12_00,h_13_00,h_14_00,h_15_00,h_16_00,h_17_00,h_18_00,h_19_00,h_20_00,estado,tipo from horarioatencion where estado=$1 and ideoficina=$2';
          const values = ['A', req.body.codigo_of];

          pool.query(sql,values, (err, result) => {

                if(err){
                  console.log(err);
                  var info = {'status':'fail'};
                  return res.json(info);

                }else{

                  //console.log(result.rows);

                  info = {'status':'ok','datos':result.rows};
                  return res.json(info);
                }

                pool.end();


          });
        })();

    }else{

       info = {'status':'fail'};
       return res.json(info);
    }



});


app.post('/admin/lista_oficina_filtro', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          const sql = 'select ideoficina, ideempresa, nombre, direccion, telefono, estado, coddist, coordenada_x, coordenada_y, ideempresa from oficina where estado=$1 and ideoficina!=$2';
          const values = ['A', req.body.idoficina];

          pool.query(sql,values, (err, result) => {

                if(err){

                  var info = {'status':'fail'};
                  return res.json(info);

                }else{

                  //console.log(result.rows);

                  info = {'status':'ok','datos':result.rows};
                  return res.json(info);
                }

                pool.end();


          });
        })();

    }else{

       info = {'status':'fail'};
       return res.json(info);
    }



});

app.post('/admin/edit_opt_horarios', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          var fecha_act = showDate();
          var hora_envio = showTime();

          //const text = 'INSERT INTO empresa(nombre,direccion,telefono,correo1,correo2,correo3,coordenada_x,coordenada_y,coddist,estado,feccrea,horcrea,fecmodif,hormodif,usrcrea,usrmodif) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16) RETURNING *'
          const text = 'update horarioatencion set h_7_00=$1, h_8_00=$2, h_9_00=$3, h_10_00=$4, h_11_00=$5, h_12_00=$6, h_13_00=$7, h_14_00=$8, h_15_00=$9, h_16_00=$10, h_17_00=$11, h_18_00=$12, h_19_00=$13, h_20_00=$14, tipo=$15  where idehorario=$16';

          const values = [req.body.h1_1,req.body.h2_1,req.body.h3_1,req.body.h4_1,req.body.h5_1,req.body.h6_1,req.body.h7_1,req.body.h8_1,req.body.h9_1,req.body.h10_1,req.body.h11_1,req.body.h12_1,req.body.h13_1,req.body.h14_1,req.body.estado_1,req.body.idehorario_1];

          // callback
          pool.query(text, values, (err, result) => {

            if (err) {
               console.log(err)
              console.log(err.stack)
              info = {'status':'fail','datos':''};
              return res.json(info);

            } else {


              const text_1 = 'update horarioatencion set h_7_00=$1, h_8_00=$2, h_9_00=$3, h_10_00=$4, h_11_00=$5, h_12_00=$6, h_13_00=$7, h_14_00=$8, h_15_00=$9, h_16_00=$10, h_17_00=$11, h_18_00=$12, h_19_00=$13, h_20_00=$14, tipo=$15  where idehorario=$16';

              const values_1 = [req.body.h1_2,req.body.h2_2,req.body.h3_2,req.body.h4_2,req.body.h5_2,req.body.h6_2,req.body.h7_2,req.body.h8_2,req.body.h9_2,req.body.h10_2,req.body.h11_2,req.body.h12_2,req.body.h13_2,req.body.h14_2,req.body.estado_2,req.body.idehorario_2];

              // callback
              pool.query(text_1, values_1, (err, result) => {

                if (err) {
                   console.log(err)
                  console.log(err.stack)
                  info = {'status':'fail','datos':''};
                  return res.json(info);

                } else {


                  info = {'status':'ok','datos':''};
                  return res.json(info);

                }

              });

            }

          });

       })();


    }else{
       res.render('pages/admin/login');
    }


});


/* -----  ----- */

/* Modulo Paquetes */
app.get('/admin/paquetes', function (req, res) {

    if(req.session.coduser){
        res.render('pages/admin/paquetes', { username: req.session.username, perfil: req.session.perfil });
    }else{
       res.render('pages/admin/login');
    }


});



app.post('/admin/lista_paquetes', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          const sql = 'select o.nombre, o.precio, o.idepaquete, o.estado ,e.nombre as empresa, o.tipo from paquetes o inner join empresa e on e.ideempresa=o.ideempresa';
          const values = [];

          pool.query(sql,values, (err, result) => {

                if(err){
                  console.log(err)
                  var info = {'status':'fail'};
                  return res.json(info);

                }else{

                  //console.log(result.rows);

                  info = {'status':'ok','datos':result.rows};
                  return res.json(info);
                }

                pool.end();


          });
        })();

    }else{

       info = {'status':'fail'};
       return res.json(info);
    }


});



app.post('/admin/new_paquete', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);


          var fecha_act = showDate();
          var hora_envio = showTime();

          const text = 'INSERT INTO paquetes(nombre, precio, ideempresa, estado,feccrea,horcrea,fecmodif,hormodif,usrcrea,usrmodif,tipo) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11) RETURNING *'
          const values = [req.body.nombre, req.body.precio, req.body.cod_empresa, "I",fecha_act,hora_envio,fecha_act,hora_envio,req.session.coduser,req.session.coduser,req.body.cod_tipo]

          // callback
          pool.query(text, values, (err, result) => {

            if (err) {
               console.log(err)
              console.log(err.stack)
              info = {'status':'fail','datos':''};
              return res.json(info);

            } else {

              info = {'status':'ok','datos':''};
              return res.json(info);

            }


          });

       })();


    }else{
       res.render('pages/admin/login');
    }


});


app.post('/admin/delete_paquete', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          const text = 'delete from paquetes where idepaquete=$1'
          const values = [req.body.codigo]

          // callback
          pool.query(text, values, (err, result) => {

            if (err) {
              console.log(err)
              console.log(err.stack)
              info = {'status':'fail','datos':''};
              return res.json(info);

            } else {

              const text_h = 'delete from producto where idepaquete=$1'
              const values_h = [req.body.codigo]

              pool.query(text_h, values_h, (err, result) => {

                if (err) {

                  console.log(err)
                  console.log(err.stack)

                  info = {'status':'fail','datos':''};
                  return res.json(info);

                } else {


                  info = {'status':'ok','datos':''};
                  return res.json(info);

                }

              });



            }

          });

       })();


    }else{
       res.render('pages/admin/login');
    }


});



app.post('/admin/get_paquete', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          const sql = 'select ideempresa, nombre, precio, tipo from paquetes where idepaquete=$1';
          const values = [req.body.codigo];

          pool.query(sql,values, (err, result) => {

                if(err){

                  var info = {'status':'fail'};
                  return res.json(info);

                }else{

                  //console.log(result.rows);

                  info = {'status':'ok','datos':result.rows};
                  return res.json(info);
                }

                pool.end();


          });
        })();

    }else{

       info = {'status':'fail'};
       return res.json(info);
    }



});



app.post('/admin/edit_paquetes', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          var fecha_act = showDate();
          var hora_envio = showTime();


          const text = 'update paquetes set nombre=$1, precio=$2, ideempresa=$3, tipo=$4 where idepaquete=$5';
          const values = [req.body.nombre, req.body.precio, req.body.cod_empresa, req.body.cod_tipo, req.body.codigo];

          // callback
          pool.query(text, values, (err, result) => {

            if (err) {
               console.log(err)
              console.log(err.stack)
              info = {'status':'fail','datos':''};
              return res.json(info);

            } else {

              //console.log(res.rows[0])
              // { name: 'brianc', email: 'brian.m.carlson@gmail.com' }
              info = {'status':'ok','datos':''};
              return res.json(info);

            }

          });

       })();


    }else{
       res.render('pages/admin/login');
    }


});

app.post('/admin/estado_paquete', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          var fecha_act = showDate();
          var hora_envio = showTime();


          const text = 'update paquetes set estado=$1 where idepaquete=$2';
          const values = [req.body.estado, req.body.codigo];

          // callback
          pool.query(text, values, (err, result) => {

            if (err) {
               console.log(err)
              console.log(err.stack)
              info = {'status':'fail','datos':''};
              return res.json(info);

            } else {

              //console.log(res.rows[0])
              // { name: 'brianc', email: 'brian.m.carlson@gmail.com' }
              info = {'status':'ok','datos':''};
              return res.json(info);

            }

          });

       })();


    }else{
       res.render('pages/admin/login');
    }


});



/* -----  ----- */


/* Modulo Productos */
app.get('/admin/productos', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          const sql = 'select ideempresa, nombre, precio from paquetes where idepaquete=$1';
          const values = [req.query.codigo];

          pool.query(sql,values, (err, result) => {

                if(err){

                  res.render('pages/admin/paquetes');

                }else{

                    var codigo = req.query.codigo;
                    res.render('pages/admin/productos', { username: req.session.username, perfil: req.session.perfil, codigo_pa: codigo, nombre_pa: result.rows[0].nombre });

                }

                pool.end();


          });
        })();

    }else{
       res.render('pages/admin/login');
    }


});



app.post('/admin/lista_productos', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          const sql = 'select nombrecorto, nombrelargo, ideproducto from producto where idepaquete=$1';
          const values = [req.body.codigo];

          pool.query(sql,values, (err, result) => {

                if(err){
                  console.log(err)
                  var info = {'status':'fail'};
                  return res.json(info);

                }else{

                  //console.log(result.rows);

                  info = {'status':'ok','datos':result.rows};
                  return res.json(info);
                }

                pool.end();


          });
        })();

    }else{

       info = {'status':'fail'};
       return res.json(info);
    }


});



app.post('/admin/new_producto', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);


          var fecha_act = showDate();
          var hora_envio = showTime();

          const text = 'INSERT INTO producto(nombrecorto, nombrelargo, idepaquete, estado,feccrea,horcrea,fecmodif,hormodif,usrcrea,usrmodif) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10) RETURNING *'
          const values = [req.body.nombre_c, req.body.nombre_l, req.body.codigo_pa, "A",fecha_act,hora_envio,fecha_act,hora_envio,req.session.coduser,req.session.coduser]

          // callback
          pool.query(text, values, (err, result) => {

            if (err) {
               console.log(err)
              console.log(err.stack)
              info = {'status':'fail','datos':''};
              return res.json(info);

            } else {

              info = {'status':'ok','datos':''};
              return res.json(info);

            }


          });

       })();


    }else{
       res.render('pages/admin/login');
    }


});


app.post('/admin/lista_paquetes_filtro', function (req, res) {

     if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          const sql = 'select idepaquete, nombre from paquetes where idepaquete!=$1';
          const values = [req.body.idepaquete];

          pool.query(sql,values, (err, result) => {

                if(err){

                  var info = {'status':'fail'};
                  return res.json(info);

                }else{

                  //console.log(result.rows);

                  info = {'status':'ok','datos':result.rows};
                  return res.json(info);
                }

                pool.end();


          });
        })();

    }else{

       info = {'status':'fail'};
       return res.json(info);
    }


});


/* -----  ----- */


/* Modulo Ventas */
app.get('/admin/ventas', function (req, res) {

    if(req.session.coduser){


        res.render('pages/admin/ventas', { username: req.session.username, perfil: req.session.perfil });
    }else{
       res.render('pages/admin/login');
    }


});

/* -----  ----- */



/* Modulo usuarios */

app.get('/admin/usuarios', function (req, res) {

    if(req.session.coduser){


        res.render('pages/admin/usuarios', { username: req.session.username, perfil: req.session.perfil });
    }else{
       res.render('pages/admin/login');
    }


});

app.post('/admin/get_usuario', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          const sql = 'select u.ideusuario, u.nombres, u.correo, u.ideempresa, u.ideperfil, u.login, u.password from usuario u where u.estado=$1 and u.ideusuario=$2';
          const values = ['A', req.body.codigo];

          pool.query(sql,values, (err, result) => {

                if(err){

                  var info = {'status':'fail'};
                  return res.json(info);

                }else{

                  //console.log(result.rows);

                  info = {'status':'ok','datos':result.rows};
                  return res.json(info);
                }

                pool.end();


          });
        })();

    }else{

       info = {'status':'fail'};
       return res.json(info);
    }



});


app.post('/admin/lista_usuarios', function (req, res) {


    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          const sql = 'select u.ideusuario, u.nombres,u.correo, p.nombre as perfil from usuario u inner join perfil p on p.ideperfil=u.ideperfil where u.estado=$1';
          const values = ['A'];

          pool.query(sql,values, (err, result) => {

                if(err){
                  console.log(err)
                  var info = {'status':'fail'};
                  return res.json(info);

                }else{

                  //console.log(result.rows);

                  info = {'status':'ok','datos':result.rows};
                  return res.json(info);
                }

                pool.end();


          });
        })();

    }else{

       info = {'status':'fail'};
       return res.json(info);
    }


});

app.post('/admin/new_usuarios', function (req, res) {

     if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          //const text = 'INSERT INTO usuario(nombres, ideempresa, ideperfil, correo, password,user,estado,feccrea,horcrea,fecmodif,hormodif,usrcrea,usrmodif) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13) RETURNING *'
          //const values = [req.body.nombre, req.body.cod_empresa, req.body.perfil, req.body.correo, req.body.user, req.body.password, "A","18-01-2018","12:06:00","18-01-2018","12:06:00",1,1]
          const text = 'INSERT INTO usuario(login,nombres,ideempresa,ideperfil,correo,password,estado,feccrea,horcrea,fecmodif,hormodif,usrcrea,usrmodif) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13) RETURNING *'
          const values = [req.body.user,req.body.nombre,req.body.cod_empresa,req.body.perfil,req.body.correo,req.body.password,"A","18-01-2018","12:06:00","18-01-2018","12:06:00",1,1]
          // callback
          pool.query(text, values, (err, result) => {

            if (err) {
               console.log(err)
              console.log(err.stack)
              info = {'status':'fail','datos':''};
              return res.json(info);

            } else {

              info = {'status':'ok','datos':''};
              return res.json(info);

            }

          });

       })();


    }else{
       res.render('pages/admin/login');
    }



});

app.post('/admin/edit_usuarios', function (req, res) {

    if(req.session.coduser){

        (async function() {


          console.log(req.body.codigo);

          const pool = new pg.Pool(opt_db);

          //const text = 'INSERT INTO empresa(nombre,direccion,telefono,correo1,correo2,correo3,coordenada_x,coordenada_y,coddist,estado,feccrea,horcrea,fecmodif,hormodif,usrcrea,usrmodif) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16) RETURNING *'
          const text = 'update usuario set nombres=$1,correo=$2,ideempresa=$3,ideperfil=$4,login=$5,password=$6 where ideusuario=$7';
          const values = [req.body.nombre, req.body.correo, req.body.cod_empresa, req.body.perfil, req.body.user, req.body.password, req.body.codigo];

          // callback
          pool.query(text, values, (err, result) => {

            if (err) {
               console.log(err)
              console.log(err.stack)
              info = {'status':'fail','datos':''};
              return res.json(info);

            } else {

              //console.log(res.rows[0])
              // { name: 'brianc', email: 'brian.m.carlson@gmail.com' }
              info = {'status':'ok','datos':''};
              return res.json(info);

            }

          });

       })();


    }else{
       res.render('pages/admin/login');
    }


});


app.post('/admin/delete_usuario', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          const text = 'delete from usuario where ideusuario=$1'
          const values = [req.body.codigo]

          // callback
          pool.query(text, values, (err, result) => {

            if (err) {
              console.log(err)
              console.log(err.stack)
              info = {'status':'fail','datos':''};
              return res.json(info);

            } else {

              info = {'status':'ok','datos':''};
              return res.json(info);

            }

          });

       })();


    }else{
       res.render('pages/admin/login');
    }


});

/* -----  ----- */

/* Modulo perfil */

app.post('/admin/get_perfiles', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          const sql = 'select ideperfil, nombre from perfil where estado=$1';
          const values = ['A'];

          pool.query(sql,values, (err, result) => {

                if(err){

                  var info = {'status':'fail'};
                  return res.json(info);

                }else{

                  //console.log(result.rows);

                  info = {'status':'ok','datos':result.rows};
                  return res.json(info);
                }

                pool.end();


          });
        })();

    }else{

       info = {'status':'fail'};
       return res.json(info);
    }

});

/* servicios*/
app.post('/lista_distritos', function(req, res) {

    (async function() {

        const pool = new pg.Pool(opt_db);
        var consulta = "select id,name from distritos where  iddepartamento = '150000' ";
        if(req.body.filtro=="1"){
            consulta+=" and recojo=1";
        }
        consulta+=" order by name asc";

        pool.query(consulta, (err, result) => {

            if(err){
                var info = {'status':'fail', 'data':''};
                res.json(info);
            }else{
                var info = {'status':'ok', 'data':result.rows};
                res.json(info);
            }

            pool.end();

        });
    })();

});

//listado de paquetes home
app.post('/listado_paquetes_index', function(req, res) {
    (async function() {

        const pool = new pg.Pool(opt_db);

        pool.query("select * from paquetes where estado='A' and tipo='P'", (err, result) => {

            if(err){
                var info = {'status':'fail', 'data':''};
                res.json(info);
            }else{
                var info = {'status':'ok', 'data':result.rows};
                res.json(info);
            }

            pool.end();

        });
    })();

});

//listado de paquetes
app.post('/listado_paquetes', function(req, res) {
    (async function() {

        const pool = new pg.Pool(opt_db);

        pool.query("select * from paquetes where estado='A' ", (err, result) => {

            if(err){
                var info = {'status':'fail', 'data':''};
                res.json(info);
            }else{
                var info = {'status':'ok', 'data':result.rows};
                res.json(info);
            }

            pool.end();

        });
    })();

});

//listado de paquetes
app.post('/get_paquete/:idpaquete', function(req, res) {
    var idpaquete = req.params.idpaquete;
    (async function() {

        const pool = new pg.Pool(opt_db);

        pool.query("select p.*,pr.nombrecorto from paquetes p inner join producto pr on p.idepaquete = pr.idepaquete where p.idepaquete = "+idpaquete, (err, result) => {

            if(err){
                var info = {'status':'fail', 'data':''};
                res.json(info);
            }else{
                var info = {'status':'ok', 'data':result.rows};
                res.json(info);
            }

            pool.end();

        });
    })();

});

//listado de productos
app.post('/listado_producto/:idpaquete', function(req, res) {
    var idpaquete = req.params.idpaquete;
    (async function() {

        const pool = new pg.Pool(opt_db);

        pool.query("select * from producto where idepaquete="+idpaquete, (err, result) => {

            if(err){
                var info = {'status':'fail', 'data':''};
                res.json(info);
            }else{
                var info = {'status':'ok', 'data':result.rows};
                res.json(info);
            }

            pool.end();

        });
    })();

});

app.get('/modelo_autos', function(req, res) {

    //info = config.modelo_auto;
    var info = {'status':'ok', 'data':config.modelo_auto};
    res.json(info);

});

app.post('/pago_stripe', function(req, res) {
    var token = req.body.stripeToken;
    var amount = req.body.amount;
    var name = req.body.name;
    var email = req.body.stripeEmail;
    var codigoventa = req.body.codigoventa;
    //     //console.log(req.body);

    var charge = stripe.charges.create({
        amount: amount, // create a charge for 1700 cents USD ($17)
        currency: 'pen',
        description: name,
        source: token,
        receipt_email: email,
    }, function(err, charge) {
        if (err) { console.warn(err) } else {


            //enviar correo

            let transporter = nodeMailer.createTransport({
                host: 'smtp.autosmart.pe',
                secure: false, //disable SSL
                requireTLS: true, //Force TLS
                tls: {
                    rejectUnauthorized: false
                },
                port: 587, //Port of STMP service
                auth: {
                    user: 'autosmart_c@autosmart.pe',
                    pass: '#kpNlbs3'
                }
            });

            // setup email data with unicode symbols
            let mailOptions = {
                from: '"AutoSmart" <autosmart_c@autosmart.pe>', // sender address
                to: email, // list of receivers
                subject: "Datos de Compra - Autosmart.pe",
                text: "Mensaje",
                html: '<p><b>Informe de Pago Stripe</b></p><p>Pago realizado satisfactoriamente.</p><p><a href="'+charge.receipt_url+'">Haga clic aqui para ver su Factura</a> </p>'
            };

            ///

            transporter.sendMail(mailOptions, (error, info) => {
                var info = {'status':'fail'};
                if (error) {
                    //return res.json(info);
                }
                //info = {'status':'ok'};
                //return res.json(info);
                //res.status(200).send(charge.receipt_url)

                // guardar
                (async function() {

                    const pool = new pg.Pool(opt_db);

                    var fecha_act = showDate();
                    var hora_envio = showTime();

                    const text = 'update ventas set codigo_strike=$1,estadopago=$2,estado=$3 where ideventa=$4';
                    const values = [charge.id,"P","A",codigoventa];

                    // callback
                    pool.query(text, values, (err, result) => {

                        if (err) {
                            info = {'status':'fail','datos':''};
                            return res.json(info);

                        } else {

                            //console.log(res.rows[0])
                            // { name: 'brianc', email: 'brian.m.carlson@gmail.com' }
                            //info = {'status':'ok','datos':''};
                            //return res.json(info);
                            res.redirect('/servicios');

                        }

                    });

                })();



            });
        }
    })
});

app.post('/buscar_oficina_ca', function (req, res) {


    (async function() {
        var tipo = req.body.tipo;

        const pool = new pg.Pool(opt_db);

        const sql = 'select * from oficina where tipo=\''+tipo+'\'';

        pool.query(sql, (err, result) => {

            if(err){
                var info = {'status':'fail'};
                return res.json(info);
            }else{
                //info = {'status':'ok','data':result.rows};
                info = {'status':'ok','data':result.rows[0]};

                return res.json(info);
            }

            pool.end();

        });
    })();
});

app.post('/buscar_oficina', function (req, res) {


    (async function() {
        var latitud_orig = req.body.latitudSrc;
        var longitud_orig = req.body.longitudSrc;
        //var latitud_orig = -16.4181031;
        //var longitud_orig = -71.49926060000001;

        const pool = new pg.Pool(opt_db);

        const sql = 'select * from oficina ';

        pool.query(sql, (err, result) => {

            if(err){
                var info = {'status':'fail'};
                return res.json(info);
            }else{
                //info = {'status':'ok','data':result.rows};
                var oficinas = result.rows;
                var dist_menor = 0;
                var count = 0;
                var codoficina = 0;
                var id_count = 0;

                oficinas.forEach(function(element) {
                    var dist = distancia.getKilometros (latitud_orig,longitud_orig,element.latitud,element.longitud);
                    //console.log("Distancia: " + dist);
                    if(count == 0){
                        dist_menor = dist;
                        codoficina = element.ideoficina;
                        id_count = count;
                    }else{
                        if(dist<dist_menor){
                            dist_menor = dist;
                            codoficina = element.ideoficina;
                            id_count = count;
                        }
                    }
                    count++;
                });
                //console.log("Distancia: " + dist_menor+" codoficina: "+codoficina);
                info = {'status':'ok','data':result.rows[id_count]};

                return res.json(info);
            }

            pool.end();

        });
    })();
});

app.get('/buscar_oficinas_x_d', function (req, res) {


    (async function() {

        const pool = new pg.Pool(opt_db);

        const sql = 'select * from oficina ';

        pool.query(sql, (err, result) => {

            if(err){
                var info = {'status':'fail'};
                return res.json(info);
            }else{
                //info = {'status':'ok','data':result.rows};
                var oficinas = result.rows;
                var latitud_orig = -16.4181031;
                var longitud_orig = -71.49926060000001;
                var dist_menor = 0;
                var count = 0;
                var codoficina = 0;
                var id_count = 0;

                oficinas.forEach(function(element) {
                    var dist = distancia.getKilometros (latitud_orig,longitud_orig,element.latitud,element.longitud);
                    //console.log("Distancia: " + dist);
                    if(count == 0){
                        dist_menor = dist;
                        codoficina = element.ideoficina;
                        id_count = count;
                    }else{
                        if(dist<dist_menor){
                            dist_menor = dist;
                            codoficina = element.ideoficina;
                            id_count = count;
                        }
                    }
                    count++;
                });
                //console.log("Distancia: " + dist_menor+" codoficina: "+codoficina);
                info = {'status':'ok','data':result.rows[id_count]};

                return res.json(info);
            }

            pool.end();

        });
    })();
});

app.post('/new_cliente', function (req, res) {
    (async function() {

        const pool = new pg.Pool(opt_db);

        var fecha_act = showDate();
        var hora_envio = showTime();

        const text = 'INSERT INTO cliente(nombres,nrodocumento,direccion,idedistrito,correo,telefono,feccrea,horcrea,estado) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9) RETURNING *';
        const values = [req.body.nombres,req.body.dni,req.body.direccion,req.body.iddistrito,req.body.correo,req.body.telefono,fecha_act,hora_envio,"A"];

        pool.query(text, values, (err, result) => {

            if (err) {
                console.log(err);
                console.log(err.stack);
                info = {'status':'fail','datos':''};
                return res.json(info);

            } else {
                info = {'status':'ok','datos':result.rows};
                return res.json(info);

            }

        });

    })();
});

app.post('/new_venta', function (req, res) {


    (async function() {

        const pool = new pg.Pool(opt_db);

        var fecha_act = showDate();
        var hora_envio = showTime();
        var tipo_auto = req.body.tipo_auto;


        const text = 'INSERT INTO ventas(idecliente,ideoficina,estadopago,tipoauto,idemodeloauto,tiporecojo,idepaquete,fechareserva,observacion,feccrea,horcrea,fecmodif,hormodif,estado,codigo_strike,tipo,h7_00,h8_00,h9_00,h10_00,h11_00,h12_00,h13_00,h14_00,h15_00,h16_00,h17_00,h18_00,h19_00,h20_00) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16, $17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30) RETURNING *';
        const values = [req.body.id_cliente,req.body.id_oficina,"R",tipo_auto,req.body.modelo_auto,0,req.body.id_paquete,req.body.fecha_reserva,"",fecha_act,hora_envio,fecha_act,hora_envio,"S","",req.body.tipo_paquete,req.body.h7_00,req.body.h8_00,req.body.h9_00,req.body.h10_00,req.body.h11_00,req.body.h12_00,req.body.h13_00,req.body.h14_00,req.body.h15_00,req.body.h16_00,req.body.h17_00,req.body.h18_00,req.body.h19_00,req.body.h20_00];

        pool.query(text, values, (err, result) => {

            if (err) {
                console.log(err);
                console.log(err.stack);
                info = {'status':'fail','datos':''};
                return res.json(info);

            } else {
                info = {'status':'ok','datos':result.rows};

                // geta info ventas
                (async function() {

                    const pool = new pg.Pool(opt_db);

                    var fecha_act = showDate();
                    var hora_envio = showTime();

                    const text2 = 'select ( v.h7_00::integer + v.h8_00::integer + v.h9_00::integer + v.h10_00::integer + v.h11_00::integer + v.h12_00::integer + v.h13_00::integer + v.h14_00::integer + v.h15_00::integer + v.h16_00::integer + v.h17_00::integer + v.h18_00::integer + v.h19_00::integer + v.h20_00::integer) as total_horas,\n' +
                        'c.nombres as cliente_nombre,c.nrodocumento as cliente_dni,c.direccion as cliente_direccion,c.correo as email,v.*, o.nombre as nom_oficina, o.direccion as dir_oficina,p.nombre as paquete, p.precio \n' +
                        'from ventas v\n' +
                        'inner join cliente c on v.idecliente = c.idecliente\n' +
                        'inner join oficina o on o.ideoficina = v.ideoficina\n' +
                        'inner join paquetes p on p.idepaquete = v.idepaquete\n' +
                        'where v.ideventa=$1';
                    const values = [result.rows[0].ideventa];

                    pool.query(text2, values, (err, result2) => {

                        if (err) {
                            console.log(err);
                            console.log(err.stack);
                            info2 = {'status':'fail','datos':''};
                            return res.json(info2);

                        } else {
                            info2 = {'status':'ok','datos':result2.rows};

                            (async function() {

                                const pool = new pg.Pool(opt_db);

                                var fecha_act = showDate();
                                var hora_envio = showTime();

                                const text3 = 'select nombrecorto,\n' +
                                    'CASE\n' +
                                    '    WHEN nombrecorto=\'Con recojo a domicilio\' THEN \'S\'    \n' +
                                    '    WHEN nombrecorto=\'Sin recojo a domicilio\' THEN \'N\' \n' +
                                    '    ELSE \'O\'\n' +
                                    '  END \n' +
                                    '  AS recojo\n' +
                                    ' from producto where idepaquete = $1 ';
                                const values3 = [result2.rows[0].idepaquete];

                                pool.query(text3, values3, (err3, result3) => {

                                    if (err3) {
                                        console.log(er3r);
                                        console.log(err3.stack);
                                        info3 = {'status':'fail','datos':''};
                                        return res.json(info3);

                                    } else {
                                        info3 = {'status':'ok','datos':result3.rows};
                                        var productos = result3.rows;
                                        var recojo_ad ='';

                                        for (j=0;j<productos.length;j++){
                                            if(productos[j].recojo=='S' || productos[j].recojo=='N'){
                                                recojo_ad = productos[j].nombrecorto;
                                            }
                                        }

                                        var asunto = "Reserva Autosmart";
                                        msg = "<!DOCTYPE html>\n" +
                                            "<html lang=\"es\">  \n" +
                                            "  <head>    \n" +
                                            "    <title>Reserva Autosmart</title>    \n" +
                                            "    <meta charset=\"UTF-8\">    \n" +
                                            "  </head>  \n" +
                                            "  <body>    \n" +
                                            "    <header>\n" +
                                            "      <h1>Reserva Autosmart</h1>      \n" +
                                            "    </header>    \n" +
                                            "    <section>      \n" +
                                            "      <article>\n" +
                                            "        <h2>Información de la Reserva</h2>\n" +
                                            "        <p><strong>Nombre y apellidos: </strong>"+result2.rows[0].cliente_nombre+"</p>\n" +
                                            "\t\t<p><strong>Dni: </strong>"+result2.rows[0].cliente_dni+"</p>\n" +
                                            "\t\t<p><strong>Domicilio : </strong>"+result2.rows[0].cliente_direccion+"</p>\n" +
                                            //"\t\t<p><strong>Fecha y hora de venta: </strong>"+result2.rows[0].feccrea+" "+result2.rows[0].horcrea+"</p>\n" +
                                            "\t\t<p><strong>Fecha y hora de venta: </strong>"+(new Date(result2.rows[0].feccrea).toLocaleDateString())+" "+result2.rows[0].horcrea+"</p>\n" +
                                            "\t\t<p><strong>Horas compradas del paquete: </strong>"+result2.rows[0].total_horas+"</p>\n" +
                                            "\t\t<p><strong>Nombre de paquete: </strong>"+result2.rows[0].paquete+"</p>\n" +
                                            "\t\t<p><strong>Recojo a domicilio: </strong>"+recojo_ad+"</p>\n" +
                                            "\t\t<p><strong>Oficina: </strong>"+result2.rows[0].nom_oficina+" - "+result2.rows[0].dir_oficina+"</p>\n" +
                                            "\t\t<p><strong>Fecha y hora de primera cita: </strong>"+(new Date(result2.rows[0].fechareserva).toLocaleDateString())+"</p>\n" +
                                            "\t\t<p><strong>Código de venta: </strong>"+result2.rows[0].ideventa+"</p>\t\t\n" +
                                            "      </article>      \n" +
                                            "    </section>\n" +
                                            "    <footer>\n" +
                                            "      <h4>Autosmart</h4>\n" +
                                            "      <p>2019</p>\n" +
                                            "    </footer>\n" +
                                            "  </body>  \n" +
                                            "</html>";
                                        send_mail(asunto,msg,result2.rows[0].email);

                                        return res.json(info3);

                                    }

                                });

                            })();



                            //return res.json(info2);

                        }

                    });

                })();




                //return res.json(info);

            }

        });

    })();


});
/* -----  ----- */

/* servicios*/
app.post('/admin/lista_ventas', function (req, res) {

    if(req.session.coduser){

        (async function() {

          const pool = new pg.Pool(opt_db);

          const sql = 'select v.ideventa, v.tipo, v.estadopago, v.fechareserva, c.correo, v.estado, v.tipo from ventas v inner join cliente c on c.idecliente=v.idecliente  where v.estado=$1 or v.estado=$2';
          const values = ['A','S'];

          pool.query(sql,values, (err, result) => {

                if(err){

                  console.log(err);

                  var info = {'status':'fail'};
                  return res.json(info);

                }else{

                  //console.log(result.rows);

                  info = {'status':'ok','datos':result.rows};
                  return res.json(info);
                }

                pool.end();


          });
        })();

    }else{

       info = {'status':'fail'};
       return res.json(info);
    }

});

/* -----  ----- */

server = http.createServer(app);
server.listen(port);
