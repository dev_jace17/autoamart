$(document).ready(function () {


    lista_paquetes();
    get_empresas();


	$(document).on('click', '#btn_estado', function(e){
		e.preventDefault();

		var data_ = $(this).attr("rel");
		var data__ = data_.split("-");

		var codigo = data__[0];
		var estado = data__[1];



		$("#codigo").val(codigo);

		   $.ajax({
		      type: 'post',
		      url: '/admin/estado_paquete',
		      data: {
		      	'codigo': codigo,
		      	'estado': estado
		      },
		      beforeSend:function() {

		      },
		      complete:function() {

		      },
		      success: function (result) {


	            if(result.status=="ok"){

		            $("#formSend").trigger("reset");

					$("#new_edit").hide();
					$("#listado_reg").show();
					$("#titulo").html("");

		         	document.location.href="/admin/paquetes";
	         	}

		      },
		      error: function () {



		      }
		   });







	});

	$(document).on('click', '#btn_horarios', function(e){
		e.preventDefault();

		var data_ = $(this).attr("rel");
		var data__ = data_.split("-");

		var codigo = data__[0];
		var nombre = data__[1];

		document.location.href = "/admin/productos?codigo="+codigo;

	});

	$(document).on('click', '#btn_modifid', function(e){
		e.preventDefault();

		var codigo = $(this).attr("rel");

		$("#codigo").val(codigo);
		$("#type").val("edit");

		    $.ajax({
		      type: 'post',
		      url: '/admin/get_paquete',
		      data: {
		      	'codigo': codigo
		      },
		      beforeSend:function() {

		      },
		      complete:function() {

				$("#new_edit").show();
				$("#listado_reg").hide();
				$("#titulo").html("Editar Paquete");

		      },
		      success: function (result) {

	            if(result.status=="ok"){

		         	$("#nombre").val(result.datos[0].nombre);
		         	$("#cod_empresa").val(result.datos[0].ideempresa);
		         	$("#precio").val(result.datos[0].precio);
		         	$("#cod_tipo").val(result.datos[0].tipo);


	         	}

		      },
		      error: function () {



		      }
		   });



	});

	$(document).on('click', '#btn_delete', function(e){
		e.preventDefault();

		var codigo = $(this).attr("rel");

	 	if(confirm("Seguro que eliminara este paquete?")){

			$("#codigo").val(codigo);

		    $.ajax({
		      type: 'post',
		      url: '/admin/delete_paquete',
		      data: {
		      	'codigo': codigo
		      },
		      beforeSend:function() {

		      },
		      complete:function() {

		      },
		      success: function (result) {


	            if(result.status=="ok"){

		            $("#formSend").trigger("reset");

					$("#new_edit").hide();
					$("#listado_reg").show();
					$("#titulo").html("");

		         	document.location.href="/admin/paquetes";
	         	}

		      },
		      error: function () {



		      }
		   });


	 	}



	});

	$(document).on('click', '#new_reg', function(e){
		e.preventDefault();

		$("#type").val("new");

		$("#new_edit").show();
		$("#listado_reg").hide();
		$("#titulo").html("Nueva Paquete");
		$("#formSend").trigger("reset");

	});

	$(document).on('submit', '#formSend', function(e){
		e.preventDefault();

		if($("#type").val()=="new"){

	        $.post("/admin/new_paquete",$("#formSend").serialize(), function( data ) {


	               if(data.status=="ok"){

	               		alert("Se agrego correctamente")

	                  	$("#formSend").trigger("reset");

						$("#new_edit").hide();
						$("#listado_reg").show();
						$("#titulo").html("");

	 					document.location.href="/admin/paquetes";

	               }else{
	                   alert("Ocurrio un error");
	               }

	        });

		}else{

	        $.post("/admin/edit_paquetes",$("#formSend").serialize(), function( data ) {

	        		if(data.status=="ok"){

	               		alert("Se modifico correctamente")

	                  	$("#formSend").trigger("reset");

						$("#new_edit").hide();
						$("#listado_reg").show();
						$("#titulo").html("");

	 					document.location.href="/admin/paquetes";

	               }else{
	                   alert("Ocurrio un error");
	               }

	        });

		}



	});

	$(document).on('click', '#cancel', function(e){
		e.preventDefault();

		$("#new_edit").hide();
		$("#listado_reg").show();
		$("#titulo").html("");
	});


});





function lista_paquetes(){

    $.ajax({
      type: 'post',
      url: '/admin/lista_paquetes',
      data: {},
      beforeSend:function() {

      },
      complete:function() {

      },
      success: function (result) {

        //alert(result.status);
        //alert(result.datos);
		var html = '';


  	 	$.each(result.datos,function(key,row){


			html = html + ' <tr>';

		   		html = html + ' 		<th scope="row">'+row.idepaquete+'</th>';

		      	html = html + ' 		<td class="td-custom">'+row.nombre+'</td>';
		       	html = html + '			<td class="td-custom">'+row.empresa+'</td>';
		       	html = html + ' 		<td class="td-custom">'+row.precio+' S/.</td>';

          //  alert(row.tipo)

            if(row.tipo=="P"){
                html = html + ' 		<td class="td-custom">Paquete</td>';
            }

            if(row.tipo=="M"){
                html = html + ' 		<td class="td-custom">Medico</td>';
            }


            if(row.tipo=="O"){
                html = html + ' 		<td class="td-custom">Oficina</td>';
            }

            if(row.tipo=="A"){
                html = html + ' 		<td class="td-custom">Circuito Alterno</td>';
            }



		       	if(row.estado=="A"){
		       		html = html + ' 		<td class="td-custom"><img src="../produccion/img/verde.png" alt="" width="32" height="32"></td>';
		       	}else{
		       		html = html + ' 		<td class="td-custom"><img src="../produccion/img/gris.jpg" alt="" width="32" height="32"></td>';
		       	}



		       	html = html + ' 		<td class="td-custom"><a href="#" alt="Editar Produtos" id="btn_horarios" rel="'+row.idepaquete+'-'+row.nombre+'"><img src="../produccion/img/list.png" alt="" width="32" height="32"></a> <a href="#" alt="Editar Paquete" id="btn_modifid" rel="'+row.idepaquete+'"><img src="../produccion/img/edit4.png" alt="" width="32" height="32"></a>  <a href="#" alt="Eliminar Paquete" id="btn_delete" rel="'+row.idepaquete+'"><img src="../produccion/img/descarga.png" alt="" width="24" height="24"></a>';


		       	if(row.estado=="A"){
		       			html = html + ' <a href="#" id="btn_estado" alt="Desactivar Paquete" rel="'+row.idepaquete+'-I"><img src="../produccion/img/desactivar.jpg" alt="" width="32" height="32"></a>	';
		       	}else{
		       			html = html + ' <a href="#" id="btn_estado" alt="Activar Paquete" rel="'+row.idepaquete+'-A"><img src="../produccion/img/activar.jpg" alt="" width="32" height="32"></a>';
		       	}

		       	html = html + '	</td>';

			 html = html + ' 		</tr>';
		});



        $("#lista_reg").find('tbody').append(html);




      },
      error: function () {



      }
   });


}


 function get_empresas(){

    $.ajax({
      type: 'post',
      url: '/admin/get_empresas',
      data: {},
      beforeSend:function() {

      },
      complete:function() {

      },
      success: function (result) {

		var html = '';

		html = html + '<option selected>Seleccionar...</option>';

  	 	$.each(result.datos,function(key,row){

			html = html + '<option value="'+row.ideempresa+'">'+row.nombre+'</option>';

		});



        $("#cod_empresa").html(html);




      },
      error: function () {



      }
   });


}
