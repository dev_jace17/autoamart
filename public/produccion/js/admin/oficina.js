$(document).ready(function () {


    lista_oficina();
    get_empresas();
	lista_distritos();

	$(document).on('click', '#btn_horarios', function(e){
		e.preventDefault();

		var data_ = $(this).attr("rel");
		var data__ = data_.split("-");

		var codigo = data__[0];
		var nombre = data__[1];

		document.location.href = "/admin/horarios?codigo="+codigo+"&nombre="+nombre;

	});

	$(document).on('click', '#btn_modifid', function(e){
		e.preventDefault();

		var codigo = $(this).attr("rel");

		$("#codigo").val(codigo);
		$("#type").val("edit");

		    $.ajax({
		      type: 'post',
		      url: '/admin/get_oficina',
		      data: {
		      	'codigo': codigo
		      },
		      beforeSend:function() {

		      },
		      complete:function() {

				$("#new_edit").show();
				$("#listado_reg").hide();
				$("#titulo").html("Editar Oficina");

		      },
		      success: function (result) {

	            if(result.status=="ok"){

		         	$("#nombre").val(result.datos[0].nombre);
		         	$("#direccion").val(result.datos[0].direccion);
		         	$("#telefono").val(result.datos[0].telefono);
		         	$("#cod_empresa").val(result.datos[0].ideempresa);
		         	$("#distrito").val(result.datos[0].coddist);
					    $("#cordenadasx").val(result.datos[0].coordenada_x);
		         	$("#cordenadasy").val(result.datos[0].coordenada_y);

		         	$("#longitud").val(result.datos[0].longitud);
		         	$("#latitud").val(result.datos[0].latitud);

		         	$("#tipo").val(result.datos[0].tipo);
              $("#img_oficina").attr("src","/produccion/img/oficina/"+result.datos[0].foto);

              $("#fileName").val(result.datos[0].foto);

	         	}

		      },
		      error: function () {



		      }
		   });



	});

	$(document).on('click', '#btn_delete', function(e){
		e.preventDefault();

		var codigo = $(this).attr("rel");

	 	if(confirm("Seguro que eliminara esta oficina?")){

			$("#codigo").val(codigo);

		    $.ajax({
		      type: 'post',
		      url: '/admin/delete_oficina',
		      data: {
		      	'codigo': codigo
		      },
		      beforeSend:function() {

		      },
		      complete:function() {

		      },
		      success: function (result) {


	            if(result.status=="ok"){

		            $("#formSend").trigger("reset");

					$("#new_edit").hide();
					$("#listado_reg").show();
					$("#titulo").html("");

		         	document.location.href="/admin/oficina";
	         	}

		      },
		      error: function () {



		      }
		   });


	 	}



	});

	$(document).on('click', '#new_reg', function(e){
		e.preventDefault();

		$("#type").val("new");

		$("#new_edit").show();
		$("#listado_reg").hide();
		$("#titulo").html("Nueva Oficina");


	});

	$(document).on('submit', '#formSend', function(e){

		e.preventDefault();

		if($("#type").val()=="new"){

      // capture o formulário
       var form = $('#formSend')[0];
       // crie um FormData {Object}
       var data = new FormData(form);
       // caso queira adicionar um campo extra ao FormData
       // data.append("customfield", "Este é um campo extra para teste");
       // desabilitar o botão de "submit" para evitar multiplos envios até receber uma resposta

       // processar
       $.ajax({
           type: "POST",
           enctype: 'multipart/form-data',
             url: "/admin/new_oficina",
           data: data,
           processData: false, // impedir que o jQuery tranforma a "data" em querystring
           contentType: false, // desabilitar o cabeçalho "Content-Type"
           cache: false, // desabilitar o "cache"
           timeout: 600000, // definir um tempo limite (opcional)
           // manipular o sucesso da requisição
           success: function (data) {
               console.log(data);
               if(data.status=="ok"){

                  alert("Se agrego correctamente")

                  $("#formSend").trigger("reset");

                  $("#new_edit").hide();
                  $("#listado_reg").show();
                  $("#titulo").html("");

                  document.location.href="/admin/oficina";

               }else{

                   alert("Ocurrio un error");

               }

           },
           // manipular erros da requisição
           error: function (e) {
               console.log(e);
               // reativar o botão de "submit"

           }
       });


		}else{

      // capture o formulário
       var form = $('#formSend')[0];
       // crie um FormData {Object}
       var data = new FormData(form);
       // caso queira adicionar um campo extra ao FormData
       // data.append("customfield", "Este é um campo extra para teste");
       // desabilitar o botão de "submit" para evitar multiplos envios até receber uma resposta

       // processar
       $.ajax({
           type: "POST",
           enctype: 'multipart/form-data',
             url: "/admin/edit_oficina",
           data: data,
           processData: false, // impedir que o jQuery tranforma a "data" em querystring
           contentType: false, // desabilitar o cabeçalho "Content-Type"
           cache: false, // desabilitar o "cache"
           timeout: 600000, // definir um tempo limite (opcional)
           // manipular o sucesso da requisição
           success: function (data) {
               console.log(data);
               if(data.status=="ok"){

 	               		alert("Se modifico correctamente")

 	                  $("#formSend").trigger("reset");

         						$("#new_edit").hide();
         						$("#listado_reg").show();
         						$("#titulo").html("");

         	 					document.location.href="/admin/oficina";

 	               }else{
 	                   alert("Ocurrio un error");
 	               }

           },
           // manipular erros da requisição
           error: function (e) {
               console.log(e);
               // reativar o botão de "submit"

           }
       });

          /*
	        $.post("/admin/edit_oficina",$("#formSend").serialize(), function( data ) {

	        		if(data.status=="ok"){

	               		alert("Se modifico correctamente")

	                  	$("#formSend").trigger("reset");

						$("#new_edit").hide();
						$("#listado_reg").show();
						$("#titulo").html("");

	 					document.location.href="/admin/oficina";

	               }else{
	                   alert("Ocurrio un error");
	               }

	        });
          */
		}



	});

	$(document).on('click', '#cancel', function(e){
		e.preventDefault();

		$("#new_edit").hide();
		$("#listado_reg").show();
		$("#titulo").html("");
	});


});





function lista_oficina(){

    $.ajax({
      type: 'post',
      url: '/admin/lista_oficina',
      data: {},
      beforeSend:function() {

      },
      complete:function() {

      },
      success: function (result) {

        //alert(result.status);
        //alert(result.datos);
		var html = '';


  	 	$.each(result.datos,function(key,row){

		 	console.log(row.ideoficina);
			html = html + ' <tr>';

		   		html = html + ' 		<th scope="row">'+row.ideoficina+'</th>';
		      	html = html + ' 		<td class="td-custom">'+row.nombre+'</td>';
		       	html = html + '			<td class="td-custom">'+row.direccion+'</td>';
		       	html = html + ' 		<td class="td-custom">'+row.telefono+'</td>';
		       	html = html + ' 		<td class="td-custom">'+row.empresa+'</td>';
		       	html = html + ' 		<td class="td-custom"><a href="#" id="btn_horarios" rel="'+row.ideoficina+'-'+row.nombre+'"><img src="../produccion/img/horario.png" alt="" width="32" height="32"></a> <a href="#" id="btn_modifid" rel="'+row.ideoficina+'"><img src="../produccion/img/edit4.png" alt="" width="32" height="32"></a>  <a href="#" id="btn_delete" rel="'+row.ideoficina+'"><img src="../produccion/img/descarga.png" alt="" width="24" height="24"></a></td>';

			 html = html + ' 		</tr>';
		});



        $("#lista_reg").find('tbody').append(html);




      },
      error: function () {



      }
   });


}


 function get_empresas(){

    $.ajax({
      type: 'post',
      url: '/admin/get_empresas',
      data: {},
      beforeSend:function() {

      },
      complete:function() {

      },
      success: function (result) {

		var html = '';

		html = html + '<option selected>Seleccionar...</option>';

  	 	$.each(result.datos,function(key,row){

			html = html + '<option value="'+row.ideempresa+'">'+row.nombre+'</option>';

		});



        $("#cod_empresa").html(html);




      },
      error: function () {



      }
   });


}

function lista_distritos(){

	   $.ajax({
	        type: 'post',
	        url: '/lista_distritos',
	        data: {},
	        beforeSend:function() {

	        },
	        complete:function() {

	        },
	        success: function (result) {

	            //alert(result.status);
	            //alert(result.datos);
	            var html = '<option value="0" selected>Seleccionar...</option>';
	            $.each(result.data,function(key,row){
	                html = html + '<option value="'+row.id+'">'+row.name+'</option>';
	            });
	            $("#distrito").html(html);
	        },
	        error: function () {
	        }
	    });


}
