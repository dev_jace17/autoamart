 $(document).ready(function () {
    

    listar();
    get_empresas();
    get_perfil();

	$(document).on('click', '#btn_modifid', function(e){
		e.preventDefault();

		var codigo = $(this).attr("rel");

		$("#codigo").val(codigo);
		$("#type").val("edit");

		    $.ajax({
		      type: 'post',
		      url: '/admin/get_usuario',
		      data: {
		      	'codigo': codigo
		      },
		      beforeSend:function() { 
		         
		      },
		      complete:function() {

				$("#new_edit").show();
				$("#listado_reg").hide();
				$("#titulo").html("Editar Usuario");		           
		      
		      },
		      success: function (result) {
		        
	            if(result.status=="ok"){

		         	$("#nombre").val(result.datos[0].nombres);
		         	$("#correo").val(result.datos[0].correo);
		         	$("#cod_empresa").val(result.datos[0].ideempresa);
		         	$("#perfil").val(result.datos[0].ideperfil);
		         	$("#user").val(result.datos[0].login);
					$("#password").val(result.datos[0].password);

	         	
	         	}	
		     
		      },
		      error: function () {
		        
		        
		      
		      }
		   });	 

	 		
		
	});

	$(document).on('click', '#btn_delete', function(e){
		e.preventDefault();

		var codigo = $(this).attr("rel");
	 	
	 	if(confirm("Seguro que eliminara este usuario?")){

			$("#codigo").val(codigo);

		    $.ajax({
		      type: 'post',
		      url: '/admin/delete_usuario',
		      data: {
		      	'codigo': codigo
		      },
		      beforeSend:function() { 
		         
		      },
		      complete:function() {
		           
		      },
		      success: function (result) {

		        
	            if(result.status=="ok"){

		            $("#formSend").trigger("reset");
							
					$("#new_edit").hide();
					$("#listado_reg").show();
					$("#titulo").html("");

		         	document.location.href="/admin/usuarios";      
	         	}	
		     
		      },
		      error: function () {
		        
		        
		      
		      }
		   });	 


	 	}
	


	});	

	$(document).on('click', '#new_reg', function(e){
		e.preventDefault();

		$("#type").val("new");

		$("#new_edit").show();
		$("#listado_reg").hide();
		$("#titulo").html("Nueva Usuario");


	});			

	$(document).on('submit', '#formSend', function(e){
		e.preventDefault();
		
		if($("#type").val()=="new"){

	        $.post("/admin/new_usuarios",$("#formSend").serialize(), function( data ) {
	              

	               if(data.status=="ok"){
	               		
	               		alert("Se agrego correctamente")

	                  	$("#formSend").trigger("reset");
						
						$("#new_edit").hide();
						$("#listado_reg").show();
						$("#titulo").html("");

	 					document.location.href="/admin/usuarios";
	               
	               }else{
	                   alert("Ocurrio un error");
	               }
	               
	        });			

		}else{

	        $.post("/admin/edit_usuarios",$("#formSend").serialize(), function( data ) {
	              
	        		if(data.status=="ok"){

	               		alert("Se modifico correctamente")

	                  	$("#formSend").trigger("reset");
						
						$("#new_edit").hide();
						$("#listado_reg").show();
						$("#titulo").html("");

	 					document.location.href="/admin/usuarios";
	               
	               }else{
	                   alert("Ocurrio un error");
	               }
	               
	        });

		}


        
	});	

	$(document).on('click', '#cancel', function(e){
		e.preventDefault();

		$("#new_edit").hide();
		$("#listado_reg").show();
		$("#titulo").html("");		
	});		


});



 

function listar(){

    $.ajax({
      type: 'post',
      url: '/admin/lista_usuarios',
      data: {},
      beforeSend:function() { 
         
      },
      complete:function() {
           
      },
      success: function (result) {

      // alert(result.status);
       //alert(result.datos);

	  var html = '';
		
        
  	 	$.each(result.datos,function(key,row){

		 	console.log(row.ideusuario);
			html = html + ' <tr>';
	   		
		   		html = html + ' 		<th scope="row">'+row.ideusuario+'</th>';
		      	html = html + ' 		<td class="td-custom">'+row.nombres+'</td>';
		       	html = html + '			<td class="td-custom">'+row.correo+'</td>';
		       	html = html + ' 		<td class="td-custom">'+row.perfil+'</td>';
		       	html = html + ' 		<td class="td-custom">'+row.empresa+'</td>';
		       	html = html + ' 		<td class="td-custom"><a href="#" id="btn_modifid" rel="'+row.ideusuario+'"><img src="../produccion/img/edit4.png" alt="" width="32" height="32"></a>  <a href="#" id="btn_delete" rel="'+row.ideusuario+'"><img src="../produccion/img/descarga.png" alt="" width="24" height="24"></a></td>';		 	

			 html = html + ' 		</tr>';
		}); 	
       
      

        $("#lista_reg").find('tbody').append(html);




      },
      error: function () {
        
        
      
      }
   });


}
  

 function get_empresas(){

    $.ajax({
      type: 'post',
      url: '/admin/get_empresas',
      data: {},
      beforeSend:function() { 
         
      },
      complete:function() {
           
      },
      success: function (result) {

		var html = '';
		
		html = html + '<option selected>Seleccionar...</option>';

  	 	$.each(result.datos,function(key,row){

			html = html + '<option value="'+row.ideempresa+'">'+row.nombre+'</option>';
		      
		}); 	
       
      

        $("#cod_empresa").html(html);




      },
      error: function () {
        
        
      
      }
   });


} 

function get_perfil(){

     $.ajax({
      type: 'post',
      url: '/admin/get_perfiles',
      data: {},
      beforeSend:function() { 
         
      },
      complete:function() {
           
      },
      success: function (result) {

		var html = '';
		
		html = html + '<option selected>Seleccionar...</option>';

  	 	$.each(result.datos,function(key,row){

			html = html + '<option value="'+row.ideperfil+'">'+row.nombre+'</option>';
		      
		}); 	
       
      

        $("#perfil").html(html);




      },
      error: function () {
        
        
      
      }
   });

} 
