$(document).ready(function () {
    

    lista_productos();
    lista_paquetes();


   	$(document).on('change', '#lista_paquetes', function(e){
		e.preventDefault();

		var codigo = $(this).val();

		document.location.href = "/admin/productos?codigo="+codigo;
		
	}); 

	$(document).on('click', '#btn_modifid', function(e){
		e.preventDefault();

		var codigo = $(this).attr("rel");

		$("#codigo").val(codigo);
		$("#type").val("edit");

		    $.ajax({
		      type: 'post',
		      url: '/admin/get_producto',
		      data: {
		      	'codigo': codigo
		      },
		      beforeSend:function() { 
		         
		      },
		      complete:function() {

				$("#new_edit").show();
				$("#listado_reg").hide();
				$("#titulo").html("Editar Producto");		           
		      
		      },
		      success: function (result) {
		        
	            if(result.status=="ok"){

		         	$("#nombre").val(result.datos[0].nombre);
		         	$("#cod_empresa").val(result.datos[0].ideempresa);
		         	$("#precio").val(result.datos[0].precio);

	         	
	         	}	
		     
		      },
		      error: function () {
		        
		        
		      
		      }
		   });	 

	 		
		
	});

	$(document).on('click', '#btn_delete', function(e){
		e.preventDefault();

		var codigo = $(this).attr("rel");
	 	
	 	if(confirm("Seguro que eliminara este producto?")){

			$("#codigo").val(codigo);

		    $.ajax({
		      type: 'post',
		      url: '/admin/delete_producto',
		      data: {
		      	'codigo': codigo
		      },
		      beforeSend:function() { 
		         
		      },
		      complete:function() {
		           
		      },
		      success: function (result) {

		        
	            if(result.status=="ok"){

		            $("#formSend").trigger("reset");
							
					$("#new_edit").hide();
					$("#listado_reg").show();
					$("#titulo").html("");

		         	document.location.href="/admin/productos?codigo="+$("#codigo_pa").val(); 
	         	}	
		     
		      },
		      error: function () {
		        
		        
		      
		      }
		   });	 


	 	}
	


	});	

	$(document).on('click', '#new_reg', function(e){
		e.preventDefault();

		$("#type").val("new");

		$("#new_edit").show();
		$("#listado_reg").hide();
		$("#titulo").html("Nueva Producto");
		$("#formSend").trigger("reset");

	});			

	$(document).on('submit', '#formSend', function(e){
		e.preventDefault();
		
		if($("#type").val()=="new"){

	        $.post("/admin/new_producto",$("#formSend").serialize(), function( data ) {
	              

	               if(data.status=="ok"){
	               		
	               		alert("Se agrego correctamente")

	                  	$("#formSend").trigger("reset");
						
						$("#new_edit").hide();
						$("#listado_reg").show();
						$("#titulo").html("");

	 					document.location.href="/admin/productos?codigo="+$("#codigo_pa").val();
	               
	               }else{
	                   alert("Ocurrio un error");
	               }
	               
	        });			

		}else{

	        $.post("/admin/edit_producto",$("#formSend").serialize(), function( data ) {
	              
	        		if(data.status=="ok"){

	               		alert("Se modifico correctamente")

	                  	$("#formSend").trigger("reset");
						
						$("#new_edit").hide();
						$("#listado_reg").show();
						$("#titulo").html("");

	 					document.location.href="/admin/productos"+$("#codigo_pa").val();
	               
	               }else{
	                   alert("Ocurrio un error");
	               }
	               
	        });

		}


        
	});	

	$(document).on('click', '#cancel', function(e){
		e.preventDefault();

		$("#new_edit").hide();
		$("#listado_reg").show();
		$("#titulo").html("");		
	});		


});



 function lista_paquetes(){

     $.ajax({
      type: 'post',
      url: '/admin/lista_paquetes_filtro',
      data: {
      	"idepaquete":$("#codigo_pa").val()
      },
      beforeSend:function() { 
         
      },
      complete:function() {
           
      },
      success: function (result) {

		var html = '';
		
        
  	 	$.each(result.datos,function(key,row){

			html = html + ' <option value="'+row.idepaquete+'">'+row.nombre+'</option>';
	   		
		}); 	
       
      

        $("#lista_paquetes").append(html);




      },
      error: function () {
        
        
      
      }
   });	


 }

function lista_productos(){

    $.ajax({
      type: 'post',
      url: '/admin/lista_productos',
      data: {
      	'codigo':$("#codigo_pa").val()
      },
      beforeSend:function() { 
         
      },
      complete:function() {
           
      },
      success: function (result) {

		var html = '';
        
  	 	$.each(result.datos,function(key,row){

		 	
			html = html + ' <tr>';
	   		
		   		html = html + ' 		<th scope="row">'+row.ideproducto+'</th>';

		      	html = html + ' 		<td class="td-custom">'+row.nombrecorto+'</td>';
		       	html = html + '			<td class="td-custom">'+row.nombrelargo+'</td>';

		      

		       	html = html + ' 		<td class="td-custom"><a href="#" alt="Editar Paquete" id="btn_modifid" rel="'+row.ideproducto+'"><img src="../produccion/img/edit4.png" alt="" width="32" height="32"></a>  <a href="#" alt="Eliminar Paquete" id="btn_delete" rel="'+row.ideproducto+'"><img src="../produccion/img/descarga.png" alt="" width="24" height="24"></a>';


		       	html = html + '	</td>';		 	

			 html = html + ' 		</tr>';
		}); 	
       
      

        $("#lista_reg").find('tbody').append(html);




      },
      error: function () {
        
        
      
      }
   });


}
  

