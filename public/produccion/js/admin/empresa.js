$(document).ready(function () {
     
    lista_empresas();
    lista_distritos();

	$(document).on('click', '#btn_modifid', function(e){
		e.preventDefault();
		
		
		var cod_empresa = $(this).attr("rel");

		$("#ideempresa").val(cod_empresa);
		$("#type").val("edit");

		    $.ajax({
		      type: 'post',
		      url: '/admin/get_empresa',
		      data: {
		      	'cod_empresa': cod_empresa
		      },
		      beforeSend:function() { 
		         
		      },
		      complete:function() {

				$("#new_edit").show();
				$("#listado_reg").hide();
				$("#titulo").html("Editar Empresa");		           
		      
		      },
		      success: function (result) {
		        
	            if(result.status=="ok"){

		         	$("#nombre").val(result.datos[0].nombre);
		         	$("#direccion").val(result.datos[0].direccion);
		         	$("#telefono").val(result.datos[0].telefono);
		         	$("#correo1").val(result.datos[0].correo1);
		         	$("#correo2").val(result.datos[0].correo2);
		         	$("#correo3").val(result.datos[0].correo3);
		         	$("#distrito").val(result.datos[0].coddist);

		         	$("#cordenadasx").val(result.datos[0].coordenada_x);
		         	$("#cordenadasy").val(result.datos[0].coordenada_y);

	         	
	         	}	
		     
		      },
		      error: function () {
		        
		        
		      
		      }
		   });	 

	 		
		
	});

	$(document).on('click', '#btn_delete', function(e){
		e.preventDefault();

		var cod_empresa = $(this).attr("rel");
	 	
	 	if(confirm("Seguro que eliminara esta empresa?")){

			$("#ideempresa").val(cod_empresa);

		    $.ajax({
		      type: 'post',
		      url: '/admin/delete_empresa',
		      data: {
		      	'cod_empresa': cod_empresa
		      },
		      beforeSend:function() { 
		         
		      },
		      complete:function() {
		           
		      },
		      success: function (result) {

		        
	            if(result.status=="ok"){
		            $("#formSendRegEmpresa").trigger("reset");
							
					$("#new_edit").hide();
					$("#listado_reg").show();
					$("#titulo").html("");

		         	document.location.href="/admin/empresa";      
	         	}	
		     
		      },
		      error: function () {
		        
		        
		      
		      }
		   });	 


	 	}
	


	});	

	$(document).on('click', '#new_reg', function(e){
		e.preventDefault();

		$("#type").val("new");

		$("#new_edit").show();
		$("#listado_reg").hide();
		$("#titulo").html("Nueva Empresa");


	});			

	$(document).on('submit', '#formSendRegEmpresa', function(e){
		e.preventDefault();
		
		if($("#type").val()=="new"){

	        $.post("/admin/new_empresa",$("#formSendRegEmpresa").serialize(), function( data ) {
	              

	               if(data.status=="ok"){
	               		
	               		alert("Se agrego correctamente")

	                  	$("#formSendRegEmpresa").trigger("reset");
						
						$("#new_edit").hide();
						$("#listado_reg").show();
						$("#titulo").html("");

	 					document.location.href="/admin/empresa";
	               
	               }else{
	                   alert("Ocurrio un error");
	               }
	               
	        });			

		}else{

	        $.post("/admin/edit_empresa",$("#formSendRegEmpresa").serialize(), function( data ) {
	              

	               if(data.status=="ok"){

	               		alert("Se modifico correctamente")

	                  	$("#formSendRegEmpresa").trigger("reset");
						
						$("#new_edit").hide();
						$("#listado_reg").show();
						$("#titulo").html("");

	 					document.location.href="/admin/empresa";
	               
	               }else{
	                   alert("Ocurrio un error");
	               }
	               
	        });

		}


        
	});	

	$(document).on('click', '#cancel', function(e){
		e.preventDefault();

		$("#new_edit").hide();
		$("#listado_reg").show();
		$("#titulo").html("");		
	});		


});



 

function lista_empresas(){

    $.ajax({
      type: 'post',
      url: '/admin/lista_empresa',
      data: {},
      beforeSend:function() { 
         
      },
      complete:function() {
           
      },
      success: function (result) {

        //alert(result.status);
        //alert(result.datos);
		var html = '';
		
        
  	 	$.each(result.datos,function(key,row){

		 	console.log(row.ideempresa);
			html = html + ' <tr>';
	   		
		   		html = html + ' 		<th scope="row">'+row.ideempresa+'</th>';
		      	html = html + ' 		<td class="td-custom">'+row.nombre+'</td>';
		       	html = html + '			<td class="td-custom">'+row.direccion+'</td>';
		       	html = html + ' 		<td class="td-custom">'+row.telefono+'</td>';
		       	html = html + ' 		<td class="td-custom">'+row.correo1+'</td>';
		       	html = html + ' 		<td class="td-custom"><a href="#" id="btn_modifid" rel="'+row.ideempresa+'"><img src="../produccion/img/edit4.png" alt="" width="32" height="32"></a>  <a href="#" id="btn_delete" rel="'+row.ideempresa+'"><img src="../produccion/img/descarga.png" alt="" width="24" height="24"></a></td>';		 	

			 html = html + ' 		</tr>';
		}); 	
       
      

        $("#lista_reg_empresa").find('tbody').append(html);




      },
      error: function () {
        
        
      
      }
   });
	  

}

function lista_distritos(){

	   $.ajax({
	        type: 'post',
	        url: '/lista_distritos',
	        data: {},
	        beforeSend:function() {

	        },
	        complete:function() {

	        },
	        success: function (result) {

	            //alert(result.status);
	            //alert(result.datos);
	            var html = '<option value="0" selected>Seleccionar...</option>';
	            $.each(result.data,function(key,row){
	                html = html + '<option value="'+row.id+'">'+row.name+'</option>';
	            });
	            $("#distrito").html(html);
	        },
	        error: function () {
	        }
	    });			


}
 