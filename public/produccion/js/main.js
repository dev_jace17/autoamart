
        AOS.init();
        window.addEventListener('load', AOS.refresh)

        $(document).ready(function () {
            var options = {
                strings: ["Obten tu brevete AHORA!", "Licencias para conducir"],
                typeSpeed: 100,
                startDelay: 0,
                backSpeed: 10,
                backDelay: 1750,
                loop: true,
            }
            var typed = new Typed(".element", options);
        });

        $('.step-content__body').each(function () {
                // $(this).css(
                //     'height',
                //     $(this).children('div:first').height() +
                //     $(this).children('.buttons').height()
                // );

                $(this).append('<input type="number " class="step-content__body-index " value="1 " style="display:none ">');
            });

            var stepSlideIndex = 0; var stepSlideDirection = '';
            $('.step-content__body .button').click(function () {
                stepSlideIndex = $(this).parent().parent().children('.step-content__body-index').val();

                switch ($(this).text()) {
                    case 'Back': stepSlideDirection = 'back'; break;
                    case 'siguiente': stepSlideDirection = 'siguiente'; break;
                }

                if ($(this).text() != 'Send')
                    stepSlide($(this).parent().parent().parent().children('.step-content__control'),
                        $(this).parent().parent(),
                        stepSlideIndex,
                        stepSlideDirection);
            });


            function stepSlide(steps, fields, index, direction) {
                fields.children('div:nth-of-type(' + index + ')').addClass('hidden');

                switch (direction) {
                    case 'back': fields.children('.step-content__body-index').val(--index); break;
                    case 'siguiente': fields.children('.step-content__body-index').val(++index); break;
                }

                fields.children('div:nth-of-type(' + index + ')').removeClass('hidden');

                steps.children('.step' + index).addClass('activestep');

                switch (direction) {
                    case 'back': steps.children('.step' + (index + 1)).removeClass('activestep'); break;
                    case 'siguiente': steps.children('.step' + (index - 1)).removeClass('activestep'); break;
                }

                fields.css(
                    'height',
                    fields.children('div:nth-of-type(' + index + ')').height() +
                    fields.children('.buttons').height()
                );

                switch (index) {
                    case 1: fields.children('.buttons').children('.back').css('display', 'none'); break;
                    case 2: fields.children('.buttons').children('.back').css('display', 'inline-block');
                        fields.children('.buttons').children('.siguiente').css('display', 'inline-block');
                        fields.children('.buttons').children('.send').css('display', 'none'); break;
                    case 3: fields.children('.buttons').children('.back').css('display', 'inline-block');
                        fields.children('.buttons').children('.siguiente').css('display', 'inline-block');
                        fields.children('.buttons').children('.send').css('display', 'none'); break;
                    case 4: fields.children('.buttons').children('.back').css('display', 'inline-block');
                        fields.children('.buttons').children('.siguiente').css('display', 'inline-block');
                        fields.children('.buttons').children('.send').css('display', 'none'); break;
                    case 5: fields.children('.buttons').children('.back').css('display', 'inline-block');
                        fields.children('.buttons').children('.siguiente').css('display', 'inline-block');
                        fields.children('.buttons').children('.send').css('display', 'none'); break;
                }
            }