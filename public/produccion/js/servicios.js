"use strict";

(function ($) {

    /*$(".paquete_item").unbind();
    $(".paquete_item").click(function(e) {
        e.preventDefault();
        var id = $(this).attr("rel");
        $("#id_paquete").val(id);
        $("#tab_otros").trigger("click");
    });*/



    //$(document).ready(function () {
        listado_paquetes();
        //lista_distritos();
        load_modelo_auto();
    //});



})(window.jQuery);

function lista_distritos(filtro){

    $.ajax({
        type: 'post',
        url: '/lista_distritos',
        data: {
            filtro:filtro
        },
        beforeSend:function() {

        },
        complete:function() {

        },
        success: function (result) {

            //alert(result.status);
            //alert(result.datos);
            var html = '<option value="0" selected>Seleccionar...</option>';
            $.each(result.data,function(key,row){
                html = html + '<option value="'+row.id+'">'+row.name+'</option>';
            });
            $(".list_distritos").html(html);
        },
        error: function () {
        }
    });
}

function listado_paquetes(){

    $.ajax({
        type: 'post',
        url: '/listado_paquetes',
        data: {},
        beforeSend:function() {

        },
        complete:function() {

        },
        success: function (result) {


            var html1 = '';
            var html2 = '';
            var html3 = '';
            var html4 = '';
            var fila = 0;
            $.each(result.data,function(key,row){
                var html = '';
                html = html + '<article class="col-12 col-md-6  col-lg-4  table-column" data-aos="fade-up" data-aos-duration="750">';
                html = html + '    <div class="table-inner">';
                html = html + '    <div class="table-header ">';
                html = html + '    <h3>PREMIUM</h3>';
                html = html + '    </div>';
                html = html + '    <div class="price-column price">';
                html = html + '    <h4 class="amount"><sup>S/</sup>'+row.precio+'</h4>';
                html = html + '<p>'+row.nombre+'</p>';
                html = html + '</div>';
                html = html + '<div class="list">';
                html = html + '    <ul>';

                $.ajax({
                    type: 'post',
                    url: '/listado_producto/'+row.idepaquete,
                    async:false,
                    data: {},
                    success: function (resultado) {

                        $.each(resultado.data,function(key2,row2){
                            html = html + '         <li>'+row2.nombrecorto+'</li>';
                        });
                    }
                });
                html = html + '     </ul>';
                html = html + '</div>';
                html = html + '<a href="#" rel="'+row.idepaquete+'_'+row.tipo+'" class="read-more hvr-bounce-to-right paquete_item"><i class="fas fa-shopping-cart"></i>COMPRAR</a>';
                html = html + '</div>';
                html = html + '</article>';

                if(row.tipo == "P"){
                    html1 += html;
                }
                if(row.tipo == "M"){
                    html2 += html;
                }
                if(row.tipo == "A"){
                    html3 += html;
                }
                if(row.tipo == "O"){
                    html4 += html;
                }
            });



            $("#list_paquetes").html(html1);
            $("#list_paquetes_medico").html(html2);
            $("#list_paquetes_circuito").html(html3);
            $("#list_paquetes_oficina").html(html4);

            //$("#list_paquetes").show();
            //$("#pasos_paquetes").hide();

            $(".paquete_item").unbind();
            $(".paquete_item").click(function(e) {
                e.preventDefault();
                var id = $(this).attr("rel");
                var partes_id = id.split("_");
                $("#id_paquete").val(partes_id[0]);
                $("#tipo_paquete").val(partes_id[1]);
                $("#step_control_paquete").hide();
                $("#step_control_medico").hide();
                $("#step_circuito_alterno").hide();
                $("#step_paquete_oficina").hide();

                if(partes_id[1] == "P"){
                    $("#step_control_paquete").show();
                    initAutocomplete();
                }
                if(partes_id[1] == "M"){
                    $("#step_control_medico").show();
                    initAutocomplete2();
                }
                if(partes_id[1] == "A"){
                    $("#step_circuito_alterno").show();
                    initAutocomplete3();
                }
                if(partes_id[1] == "O"){
                    $("#step_paquete_oficina").show();
                    initAutocomplete4();
                }
                $("#txtSrcLatitud").val("");
                $("#txtSrcLongitud").val("");
                load_paquete();
                //$("#list_paquetes").hide();
                //$("#pasos_paquetes").show();

                $("#tab_proceso_compra").trigger("click");
            });
        },
        error: function () {
        }
    });
}

function load_paquete(){

    $.ajax({
        type: 'post',
        url: '/get_paquete/'+$("#id_paquete").val(),
        data: {},
        beforeSend:function() {

        },
        complete:function() {

        },
        success: function (result) {

            var html = '';
            var detalle = "";
            var fila = 0;
            var precio = 0;
            var descripcion = "";
            var tipo_paquete = "";
            var recojo = false;
            $.each(result.data,function(key,row){
                precio = row.precio;
                if(fila == 0){
                    descripcion = row.nombrecorto;
                    detalle = row.nombrecorto;
                    tipo_paquete = row.tipo;
                }else{
                    descripcion += ", "+row.nombrecorto;
                    detalle += "<br>"+row.nombrecorto;
                }
                if(row.nombrecorto == "Con recojo a domicilio"){
                    recojo = true;
                }


                html += '<li>';
                html += '    <span class="span_check"><i class="fas fa-check  pd-r12  color_sec"></i>'+row.nombrecorto+'</span>';
                html += '</li>';
                fila++;
            });

            if(recojo){
                lista_distritos("1");
            }else{
                lista_distritos("0");
            }
            $("#pack_title").html("PRECIO : S/. "+precio);
            $("#pack_detail").html(html);
            $(".pack_detail_title").html("Paquete "+precio);
            $(".content--title").html("Reserva : Paquete "+precio);
            $(".pack_detail_final").html(detalle);
            $("#pack_title3").html("PRECIO : S/. "+precio);
            $("#pack_detail3").html(html);
            $("#pack_title4").html("PRECIO : S/. "+precio);
            $("#pack_detail4").html(html);

            if(tipo_paquete == "M"){
                $(".pack_detail_title").html("Paquete Examen Médico "+precio);
                $(".content--title").html("Reserva : Paquete Examen Médico "+precio);
            }


            var script_stripe = "<form action='/pago_stripe' method='POST'>";
            script_stripe += "<script ";
            script_stripe += "src='https://checkout.stripe.com/checkout.js' class='stripe-button' ";
            script_stripe += "data-key='pk_test_PqLOjFLG2KBt8qXprJbwSB75' ";
            script_stripe += "data-amount='"+precio+"00' ";
            script_stripe += "data-name='Paquete "+precio+"' ";
            script_stripe += "data-description='"+descripcion+"' ";
            script_stripe += "data-image='http://autosmart.pe/produccion/img/logo_stripe.png' ";
            script_stripe += "data-currency='PEN' ";
            script_stripe += "data-locale='auto' ";
            script_stripe += "data-zip-code='true'> ";
            script_stripe += "</script>";
            script_stripe += "<input type='hidden' name='amount' value='"+precio+"00'>";
            script_stripe += "<input type='hidden' name='name' value='Paquete "+precio+"'>";
            script_stripe += "<input type='hidden' name='codigoventa' id='codigoventa' value=''>";
            //script_stripe += "<input type='hidden' name='currency' value='pen'>";
            script_stripe += "</form>";
            //alert(script_stripe);



            $("#form_pay_stripe_clases").html(script_stripe);
        },
        error: function () {
        }
    });
}
function load_modelo_auto(){

    $.ajax({
        type: 'get',
        url: '/modelo_autos/',
        data: {},
        beforeSend:function() {

        },
        complete:function() {

        },
        success: function (result) {

            var html = '<option value="0">Seleccione auto</option>';
            $.each(result.data,function(key,row){
                html += '<option value="'+key+'">'+row+'</option>';
            });
            $("#modelo_auto").html(html);
            $("#modelo_auto3").html(html);
        },
        error: function () {
        }
    });
}

function buscar_oficina(){

    $.ajax({
        type: 'post',
        url: '/buscar_oficina',
        data: {
            latitudSrc:parseFloat($("#txtSrcLatitud").val()),
            longitudSrc:parseFloat($("#txtSrcLongitud").val())
        },
        beforeSend:function() {

        },
        complete:function() {

        },
        success: function (result) {

            //alert(result.status);
            //alert(result.data[0].latitud);
            var latitud = result.data.latitud;
            var longitud = result.data.longitud;

            $("#txtSrcLongitudOficina").val(result.data.longitud);
            $("#txtSrcLatitudOficina").val(result.data.latitud);
            $("#txtSrcNombreOficina").val(result.data.nombre);
            $("#nombre_oficina").html(result.data.nombre);
            $("#imagen_oficina").attr("src","http://autosmart.pe/produccion/img/oficina/"+result.data.foto);
            $("#id_oficina").val(result.data.ideoficina);


            initialize();

        },
        error: function () {
        }
    });
}

function buscar_oficina3(){

    $.ajax({
        type: 'post',
        url: '/buscar_oficina_ca',
        data: {
            tipo:'C'
        },
        beforeSend:function() {

        },
        complete:function() {

        },
        success: function (result) {

            //alert(result.status);
            //alert(result.data[0].latitud);
            var latitud = result.data.latitud;
            var longitud = result.data.longitud;

            $("#txtSrcLongitudOficina").val(result.data.longitud);
            $("#txtSrcLatitudOficina").val(result.data.latitud);
            $("#txtSrcNombreOficina").val(result.data.nombre);
            $("#nombre_oficina3").html(result.data.nombre);
            $("#imagen_oficina3").attr("src","http://autosmart.pe/produccion/img/oficina/"+result.data.foto);
            $("#id_oficina").val(result.data.ideoficina);

            initialize3();

        },
        error: function () {
        }
    });
}

function buscar_oficina4(){

    $.ajax({
        type: 'post',
        url: '/buscar_oficina_ca',
        data: {
            tipo:'N'
        },
        beforeSend:function() {

        },
        complete:function() {

        },
        success: function (result) {

            //alert(result.status);
            //alert(result.data[0].latitud);
            var latitud = result.data.latitud;
            var longitud = result.data.longitud;

            $("#txtSrcLongitudOficina").val(result.data.longitud);
            $("#txtSrcLatitudOficina").val(result.data.latitud);
            $("#txtSrcNombreOficina").val(result.data.nombre);
            $("#nombre_oficina4").html(result.data.nombre);
            $("#imagen_oficina4").attr("src","http://autosmart.pe/produccion/img/oficina/"+result.data.foto);
            $("#id_oficina").val(result.data.ideoficina);

            initialize4();

        },
        error: function () {
        }
    });
}

function guardar_reserva(){
    var $modal = $('.js-loading-bar'),
        $bar = $modal.find('.progress-bar');

    $modal.modal('show');
    $bar.addClass('animate');

    setTimeout(function() {
        $bar.removeClass('animate');
        $modal.modal('hide');
    }, 5000);

    var tipo_paquete = $("#tipo_paquete").val();
    var h7_00 = "0";
    var h8_00 = "0";
    var h9_00 = "0";
    var h10_00 = "0";
    var h11_00 = "0";
    var h12_00 = "0";
    var h13_00 = "0";
    var h14_00 = "0";
    var h15_00 = "0";
    var h16_00 = "0";
    var h17_00 = "0";
    var h18_00 = "0";
    var h19_00 = "0";
    var h20_00 = "0";

    if(tipo_paquete == "P") {
        var iddistrito = $("#list_distritos").val();
        var direccion = $("#searchDireccion").val();
        var id_oficina = $("#id_oficina").val();
        var modelo_auto = $("#modelo_auto").val();
        var fecha_reserva = $("#datepicker").val();
        var nombres = $("#nombres").val();
        var correo = $("#correo").val();
        var telefono = $("#telefono").val();
        var dni = $("#dni").val();
        var tipo_auto = $('input[name=tipo_auto]:checked').val();

        $(".hora_pkt:checked").each(
            function() {
                //alert("El checkbox con valor " + $(this).val() + " está seleccionado");
                switch($(this).val()){
                    case "0":  h7_00 = "1"; break;
                    case "1":  h8_00 = "1"; break;
                    case "2":  h9_00 = "1"; break;
                    case "3":  h10_00 = "1"; break;
                    case "4":  h11_00 = "1"; break;
                    case "5":  h12_00 = "1"; break;
                    case "6":  h13_00 = "1"; break;
                    case "7":  h14_00 = "1"; break;
                    case "8":  h15_00 = "1"; break;
                    case "9":  h16_00 = "1"; break;
                    case "10":  h17_00 = "1"; break;
                    case "11":  h18_00 = "1"; break;
                    case "12":  h19_00 = "1"; break;
                    case "13":  h20_00 = "1"; break;

                }
            }
        );

    }
    if(tipo_paquete == "M") {
        var iddistrito = $("#list_distritos2").val();
        var direccion = $("#searchDireccion2").val();
        var id_oficina = 0;
        var modelo_auto = "";
        var fecha_reserva = $("#datepicker").val();
        var nombres = $("#nombres2").val();
        var correo = $("#correo2").val();
        var telefono = $("#telefono2").val();
        var dni = $("#dni2").val();
        var tipo_auto = "";
    }
    if(tipo_paquete == "A") {
        var iddistrito = $("#list_distritos3").val();
        var direccion = $("#searchDireccion3").val();
        var id_oficina = $("#id_oficina").val();
        var modelo_auto = $("#modelo_auto3").val();
        var fecha_reserva = $("#datepicker3").val();
        var nombres = $("#nombres3").val();
        var correo = $("#correo3").val();
        var telefono = $("#telefono3").val();
        var dni = $("#dni3").val();
        var tipo_auto = $('input[name=tipo_auto3]:checked').val();

        $(".hora_pkt3:checked").each(
            function() {
                //alert("El checkbox con valor " + $(this).val() + " está seleccionado");
                switch($(this).val()){
                    case "0":  h7_00 = "1"; break;
                    case "1":  h8_00 = "1"; break;
                    case "2":  h9_00 = "1"; break;
                    case "3":  h10_00 = "1"; break;
                    case "4":  h11_00 = "1"; break;
                    case "5":  h12_00 = "1"; break;
                    case "6":  h13_00 = "1"; break;
                    case "7":  h14_00 = "1"; break;
                    case "8":  h15_00 = "1"; break;
                    case "9":  h16_00 = "1"; break;
                    case "10":  h17_00 = "1"; break;
                    case "11":  h18_00 = "1"; break;
                    case "12":  h19_00 = "1"; break;
                    case "13":  h20_00 = "1"; break;

                }
            }
        );
    }

    if(tipo_paquete == "O") {
        var iddistrito = $("#list_distritos4").val();
        var direccion = $("#searchDireccion4").val();
        var id_oficina = $("#id_oficina").val();
        var modelo_auto = $("#modelo_auto4").val();
        var fecha_reserva = $("#datepicker4").val();
        var nombres = $("#nombres4").val();
        var correo = $("#correo4").val();
        var telefono = $("#telefono4").val();
        var dni = $("#dni4").val();
        var tipo_auto = $('input[name=tipo_auto4]:checked').val();

        $(".hora_pkt4:checked").each(
            function() {
                //alert("El checkbox con valor " + $(this).val() + " está seleccionado");
                switch($(this).val()){
                    case "0":  h7_00 = "1"; break;
                    case "1":  h8_00 = "1"; break;
                    case "2":  h9_00 = "1"; break;
                    case "3":  h10_00 = "1"; break;
                    case "4":  h11_00 = "1"; break;
                    case "5":  h12_00 = "1"; break;
                    case "6":  h13_00 = "1"; break;
                    case "7":  h14_00 = "1"; break;
                    case "8":  h15_00 = "1"; break;
                    case "9":  h16_00 = "1"; break;
                    case "10":  h17_00 = "1"; break;
                    case "11":  h18_00 = "1"; break;
                    case "12":  h19_00 = "1"; break;
                    case "13":  h20_00 = "1"; break;

                }
            }
        );
    }

    var id_paquete = $("#id_paquete").val();



    $.ajax({
        type: 'post',
        url: '/new_cliente',
        async: false,
        data: {
            direccion:direccion,
            iddistrito:iddistrito,
            nombres:nombres,
            correo:correo,
            telefono:telefono,
            dni:dni
        },
        success: function (result) {
            var id_cliente = result.datos[0].idecliente;
            var fecha_reserva_part = fecha_reserva.split("/");
            fecha_reserva = fecha_reserva_part[2]+"-"+fecha_reserva_part[1]+"-"+fecha_reserva_part[0];

            $.ajax({
                type: 'post',
                url: '/new_venta',
                async: false,
                data: {
                    id_oficina:id_oficina,
                    id_paquete:id_paquete,
                    id_cliente:id_cliente,
                    direccion:direccion,
                    iddistrito:iddistrito,
                    modelo_auto:modelo_auto,
                    nombres:nombres,
                    correo:correo,
                    telefono:telefono,
                    dni:dni,
                    fecha_reserva:fecha_reserva,
                    tipo_paquete:tipo_paquete,
                    tipo_auto:tipo_auto,
                    h7_00 :h7_00,
                    h8_00 :h8_00,
                    h9_00 :h9_00,
                    h10_00 :h10_00,
                    h11_00:h11_00,
                    h12_00:h12_00,
                    h13_00:h13_00,
                    h14_00:h14_00,
                    h15_00:h15_00,
                    h16_00:h16_00,
                    h17_00:h17_00,
                    h18_00:h18_00,
                    h19_00:h19_00,
                    h20_00:h20_00
                },
                success: function (result2) {

                    $("#codigoventa").val(result2.datos[0].ideventa);
                    $bar.removeClass('animate');
                    $modal.modal('hide');

                },
                error: function () {
                }
            });

        },
        error: function () {
        }
    });
}
function load_form_stripe(e){
    e.preventDefault();
    $("#btn_stripe").trigger("click");
}

$(".btn_load_stripe").on("click",function(e){
    e.preventDefault();
    guardar_reserva();

    $("#form_pay_stripe_clases .stripe-button-el").trigger("click");
});

$(".btn_load_reserva").on("click",function(e){
    e.preventDefault();
    guardar_reserva();
    alert("Su reserva fué satisfactoria, en breve nos pondremos en contacto");
    window.location.href = "/servicios";
});

