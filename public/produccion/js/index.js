"use strict";

(function ($) {

   listado_paquetes();

   $(document).on('click', '.paquete_item', function(e){
 		e.preventDefault();

 		document.location.href = "/servicios";

 	});


})(window.jQuery);


function listado_paquetes(){

    $.ajax({
        type: 'post',
        url: '/listado_paquetes_index',
        data: {},
        beforeSend:function() {

        },
        complete:function() {

        },
        success: function (result) {

            var html = '';
            var fila = 0;
            var conta_ = 1;

            $.each(result.data,function(key,row){

                if(conta_<=3){

                    html = html + '<article class="col-12 col-md-6  col-lg-4  table-column" data-aos="fade-up" data-aos-duration="750">';
                    html = html + '    <div class="table-inner">';
                    html = html + '    <div class="table-header ">';
                    html = html + '    <h3>PREMIUM</h3>';
                    html = html + '    </div>';
                    html = html + '    <div class="price-column price">';
                    html = html + '    <h4 class="amount"><sup>S/</sup>'+row.precio+'</h4>';
                    html = html + '<p>'+row.nombre+'</p>';
                    html = html + '</div>';
                    html = html + '<div class="list">';
                    html = html + '    <ul>';

                            $.ajax({
                        type: 'post',
                        url: '/listado_producto/'+row.idepaquete,
                        async:false,
                        data: {},
                        success: function (resultado) {

                            $.each(resultado.data,function(key2,row2){
                                html = html + '         <li>'+row2.nombrecorto+'</li>';
                            });
                        }
                    });
                    html = html + '     </ul>';
                    html = html + '</div>';
                    html = html + '<a href="/servicios" rel="'+row.idepaquete+'" class="read-more hvr-bounce-to-right paquete_item"><i class="fas fa-shopping-cart"></i>COMPRAR</a>';
                    html = html + '</div>';
                    html = html + '</article>';

                }


                conta_ = conta_+1;

            });



            $("#lista_paquetes").html(html);

            $(".paquete_item").unbind();
            $(".paquete_item").click(function(e) {
                e.preventDefault();
                var id = $(this).attr("rel");
                $("#id_paquete").val(id);
                load_paquete();
                $("#tab_otros").trigger("click");
            });
        },
        error: function () {
        }
    });
}
